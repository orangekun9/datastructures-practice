// Solution with time complexity of O(n), if all elements are continuous then O(n^2).

#include<iostream>
#include<bits/stdc++.h>
using namespace std;

int main(){
    int arr[] = {1,5,4,3,8,3,9,15,9,9};
    int val = 12;
    sort(arr,arr+10);

    // Pair sum part
    int s=0,e=9,count1,count2;

    while(s<e){
        count1=1,count2=1;
        if(arr[s]+arr[e] < val){
            s++;
        }
        else if(arr[s]+arr[e] > val){
            e--;
        }
        else if(arr[s]+arr[e] == val){
            // For continuous elements like 2 2 2 2 2 2
            if(arr[s] == arr[e]){
                count1 = e-s+1;
                int x = (count1*(count1-1))/2;
                for(int i=0;i<x;i++){
                    cout<<arr[s]<<" "<<arr[e]<<endl;
                }
                s+=count1;
                e-=count1;
            }

            // For non continuous elements
            else{
                int si=s+1,ei=e-1;
                while(si<ei){
                    if(arr[s]==arr[si]){
                        count1++;
                        si++;
                    }
                    else if(arr[e] == arr[ei]){
                        count2++;
                        ei--;
                    }
                    else{
                        si++;
                        ei--;
                    }
                }
                for(int i=0;i<(count1*count2);i++){
                    cout<<arr[s]<<" "<<arr[e]<<endl;
                }
                s+=count1;
                e-=count2;
            }
        }
    }
}