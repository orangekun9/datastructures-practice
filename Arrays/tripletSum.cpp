// Time complexity of O(n)
#include<iostream>
#include<bits/stdc++.h>
using namespace std;

int main(){
    int input[] = {1,2,3,4,5,6,7};
    int size = 7;
    int x = 12;
    sort(input, input+7);
    int s,e,count1,count2;
    for(int i=0;i<size-2;i++){
        s = i+1;
        e = size-1;
        while(s<e){
            if(input[i]+input[s]+input[e] > x){
                e--;
            }
            else if(input[i]+input[s]+input[e] < x){
                s++;
            }
            else if(input[i]+input[s]+input[e] == x){
                count1=0,count2=0;
                for(int j=s;j<=e;j++){
                    if(input[j] == input[s]){
                        count1++;
                    }
                    else{
                        break;
                    }
                }
                
                for(int j=e;j>=s;j--){
                    if(input[j] == input[e]){
                        count2++;
                    }
                    else{
                        break;
                    }
                }

                int combinations = count1*count2;
                if(input[s] == input[e]){
                    combinations = ((e-s+1)*(e-s))/2;
                }

                for(int j=0;j<combinations;j++){
                    cout<<input[i]<<" "<<input[s]<<" "<<input[e]<<endl;
                }
                s+=count1;
                e-=count2;
            }
        }  
    }
}