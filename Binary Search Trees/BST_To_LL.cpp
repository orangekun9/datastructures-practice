#include<iostream>
#include<queue>
#include "bTree.h"
using namespace std;

template <typename T>
class node{
    public:
        T data;
        node<T>* next;

        node(T data){
            this->data = data;
            next = NULL;
        }
};

BinaryTreeNode<int>* level_Wise_input(){
    int rootData;
    cout<<"Enter data: ";
    cin>>rootData;
    cout<<endl;
    BinaryTreeNode<int>* root = new BinaryTreeNode<int>(rootData);

    queue<BinaryTreeNode<int>*> pendingNodes;
    pendingNodes.push(root);

    while(pendingNodes.size() != NULL){
        BinaryTreeNode<int>* frontNode = pendingNodes.front();
        pendingNodes.pop();
        cout<<"Enter left child of "<<frontNode->data<<": ";
        int leftChildData;
        cin>>leftChildData;
        if(leftChildData != -1){
            BinaryTreeNode<int>* leftChild = new BinaryTreeNode<int>(leftChildData);
            frontNode->left = leftChild;
            pendingNodes.push(leftChild);
        }

        cout<<"\nEnter right child of "<<frontNode->data<<": ";
        int rightChildData;
        cin>>rightChildData;
        cout<<endl;
        if(rightChildData != -1){
            BinaryTreeNode<int>* rightChild = new BinaryTreeNode<int>(rightChildData);
            frontNode->right = rightChild;
            pendingNodes.push(rightChild);
        }
    }
    return root;
}

pair<node<int>*, node<int>*> BSTLL(BinaryTreeNode<int>* root){
    if(root == NULL){
        pair<node<int>*, node<int>*> p;
        p.first = NULL;
        p.second = NULL;
        return p;
    }

    pair<node<int>*, node<int>*> left = BSTLL(root->left);
    node<int>* rootNode = new node<int>(root->data);
    pair<node<int>*, node<int>*> right = BSTLL(root->right);
    
    pair<node<int>*, node<int>*> ans;

    if(left.first == NULL){
        ans.first = rootNode;
        rootNode->next = right.first;
        if(right.first == NULL){
            ans.second = rootNode;
        }
        else{
            ans.second = right.second;
        }
    }
    else{
        ans.first = left.first;
        left.second->next = rootNode;
        rootNode->next = right.first;
        ans.second = right.second;
    }

    return ans;
}

node<int>* BST_2_LL(BinaryTreeNode<int>* root){
    if(root == NULL){
        return NULL;
    }
    return BSTLL(root).first;
}


int main(){
    BinaryTreeNode<int>* root = level_Wise_input();
    node<int>* llroot = BST_2_LL(root);
    while(llroot != NULL){
        cout<<llroot->data<<" ";
        llroot = llroot->next;
    }
}