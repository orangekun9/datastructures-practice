#include<iostream>
#include "binarySearchTree.h"
using namespace std;

int main(){
    BST t;
    t.insert(6);
    t.insert(4);
    t.insert(8);
    t.insert(2);
    t.insert(5);
    t.insert(7);
    t.insert(9);

    cout<<t.hasData(500)<<endl;
    t.print();
    cout<<endl;
    t.deleteData(6);
    t.print();
}