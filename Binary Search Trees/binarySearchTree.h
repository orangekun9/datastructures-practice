#include "bTree.h"
#include<iostream>
using namespace std;
class BST{
    BinaryTreeNode<int>* root;

    bool hasData(int data, BinaryTreeNode<int>* node){
        if(node == NULL){
            return false;
        }   
        if(node->data = data){
            return true;
        }
        else if(data < node->data){
            return hasData(data, node->left);
        }
        else{
            return hasData(data, node->right);
        }
    }

    
    
    // Delete
    BinaryTreeNode<int>* deleteData(BinaryTreeNode<int>* node, int data){
        if(node == NULL){
            return node;
        }

        if(data > node->data){
            node->right = deleteData(node->right, data);
        }
        else if(data < node->data){
            node->left = deleteData(node->left, data);
        }
        else{
            if(node->left == NULL && node->right == NULL){
                delete node;
                return NULL;
            }
            else if(node->left == NULL){
                BinaryTreeNode<int>* temp = node->right;
                node->right = NULL;
                delete node;
                return temp;
            }
            else if(node->right == NULL){
                BinaryTreeNode<int>* temp = node->left;
                node->left = NULL;
                delete node;
                return temp;
            }
            else{
                BinaryTreeNode<int>* temp = node->right;
                while(temp->left != NULL){
                    temp = temp->left;
                }
                int minData = temp->data;
                node->data = minData;
                node->right = deleteData(node->right, minData);
                return node;
            }
        }
        return node;
    }
    // Insert
    BinaryTreeNode<int>* insert(int data, BinaryTreeNode<int>* node){
        if(node == NULL){
            BinaryTreeNode<int>* newNode = new BinaryTreeNode<int>(data);
            return newNode;
        }
        
        if(data < node->data){
            node->left = insert(data, node->left);
        }
        else if(data >= node->data){
            node->right = insert(data, node->right);
        }
        
        return node;
    }
    
    //Print
    void printTree(BinaryTreeNode<int>* node){
        if(node == NULL){
            return;
        }
        
        cout<<node->data<<":";
        if(node->left){
            cout<<"L: "<<node->left->data;
        }
        if(node->right){
            cout<<",R: "<<node->right->data;
        }
        cout<<endl;
        printTree(node->left);
        printTree(node->right);
    }
    
    
    public:
    
    BST(){
        root = NULL;
    }
    void deleteData(int data){
        this->root = deleteData(this->root, data);
    }

    void insert(int data){
        root = insert(data, root);
    }

    bool hasData(int data){
        return this->hasData(data, root);
    }

    void print(){
        this->printTree(root);
    }
    ~BST(){
        delete root;
    }
};