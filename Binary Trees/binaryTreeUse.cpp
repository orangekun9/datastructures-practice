#include<iostream>
using namespace std;
#include "btree.h"
#include<queue>
#include<climits>

// Print Tree
void printTree(BinaryTreeNode<int>* root){
    if(root == NULL){
        return;
    }
    cout<<root->data<<":";

    if(root->left != NULL){
        cout<<" L "<<root->left->data;
    }

    if(root->right != NULL){
        cout<<" R "<<root->right->data;
    }
    cout<<endl;
    printTree(root->left);
    printTree(root->right);
}

// Take input
BinaryTreeNode<int>* takeInput(){
    int rootData;
    cout<<"Enter data: ";
    cin>>rootData;
    cout<<endl;
    if(rootData == -1){
        return NULL;
    }
    BinaryTreeNode<int>* root = new BinaryTreeNode<int>(rootData);
    BinaryTreeNode<int>* leftChild = takeInput();
    BinaryTreeNode<int>* rightChild = takeInput();
    root->left = leftChild;
    root->right = rightChild;
    return root;
}


// Level Wise Input
BinaryTreeNode<int>* level_Wise_input(){
    int rootData;
    cout<<"Enter data: ";
    cin>>rootData;
    cout<<endl;
    BinaryTreeNode<int>* root = new BinaryTreeNode<int>(rootData);

    queue<BinaryTreeNode<int>*> pendingNodes;
    pendingNodes.push(root);

    while(pendingNodes.size() != NULL){
        BinaryTreeNode<int>* frontNode = pendingNodes.front();
        pendingNodes.pop();
        cout<<"Enter left child of "<<frontNode->data<<": ";
        int leftChildData;
        cin>>leftChildData;
        if(leftChildData != -1){
            BinaryTreeNode<int>* leftChild = new BinaryTreeNode<int>(leftChildData);
            frontNode->left = leftChild;
            pendingNodes.push(leftChild);
        }

        cout<<"\nEnter right child of "<<frontNode->data<<": ";
        int rightChildData;
        cin>>rightChildData;
        cout<<endl;
        if(rightChildData != -1){
            BinaryTreeNode<int>* rightChild = new BinaryTreeNode<int>(rightChildData);
            frontNode->right = rightChild;
            pendingNodes.push(rightChild);
        }
    }
    return root;
}


// Count Nodes
int numNodes(BinaryTreeNode<int>* root){
    if(root == NULL){
        return 0;
    }

    return 1 + numNodes(root->left) + numNodes(root->right);
}


// Traversals
void inOrderTraversal(BinaryTreeNode<int>* root){
    if(root == NULL){
        return;
    }

    inOrderTraversal(root->left);
    cout<<root->data<<" ";
    inOrderTraversal(root->right);
}


void preOrderTraversal(BinaryTreeNode<int>* root){
    if(root == NULL){
        return;
    }
    cout<<root->data<<" ";
    preOrderTraversal(root->left);
    preOrderTraversal(root->right);
}



// Build Tree from PREORDER and INORDER
BinaryTreeNode<int>* buildTreeHelper(int *inOrder, int *preOrder, int inS, int inE, int preS, int preE){
    if(inS > inE){
        return NULL;
    }

    int rootData = preOrder[preS];
    int rootIndex=0;
    while(inOrder[rootIndex] != rootData){
        rootIndex++;
    }

    int LpreS = preS + 1;
    int LinS = inS;
    int LinE = rootIndex - 1;
    int LpreE = LinE - LinS + LpreS;
    int RpreS = LpreE + 1;
    int RpreE = preE;
    int RinS = rootIndex + 1;
    int RinE = inE;
    BinaryTreeNode<int>* root = new BinaryTreeNode<int>(rootData);

    root->left = buildTreeHelper(inOrder, preOrder, LinS, LinE, LpreS, LpreE);
    root->right = buildTreeHelper(inOrder, preOrder, RinS, RinE, RpreS, RpreE);
    return root;
}

BinaryTreeNode<int>* buildTree(int *inOrder, int *preOrder, int size){
    return buildTreeHelper(inOrder, preOrder, 0, size - 1, 0, size - 1);
}



// Diameter of a Binary Tree
int height(BinaryTreeNode<int>* root){
    if(root == NULL){
        return 0;
    }
    return 1+max(height(root->left), height(root->right));
}

int diameter(BinaryTreeNode<int>* root){
    if(root == NULL){
        return 0;
    }
    int option1 = height(root->left) + height(root->right);
    int option2 = diameter(root->left);
    int option3 = diameter(root->right);

    return max(option1, max(option2, option3));
}

// Better approach to find the diameter of the tree
pair<int, int> heightDiameter(BinaryTreeNode<int>* root){
    if(root == NULL){
        pair<int, int> p;
        p.first = 0;
        p.second = 0;
        return p;
    }

    pair<int,int> leftAns = heightDiameter(root->left);
    pair<int,int> rightAns = heightDiameter(root->right);

    int lh = leftAns.first;
    int ld = leftAns.second;
    int rh = rightAns.first;
    int rd = rightAns.second;

    int height = 1 + max(lh,rh);
    int diameter = 1 + max(lh+rh, max(ld, rd));

    pair<int, int> p;
    p.first = height;
    p.second = diameter;

    return p;
}

// Sum of all nodes in Binary Tree
int sumOfAllNodes(BinaryTreeNode<int>* root){
    if(root == NULL){
        return 0;
    }

    return root->data + sumOfAllNodes(root->left) + sumOfAllNodes(root->right);
}


// Check if the tree is BST or not
class isBSTReturn{
    public:
        bool isBST;
        int minimum;
        int maximum;
};

isBSTReturn isBST2(BinaryTreeNode<int>* root){
    if(root == NULL){
        isBSTReturn b;
        b.isBST = true;
        b.minimum = INT_MAX;
        b.maximum = INT_MIN;
        return b;
    }

    isBSTReturn left = isBST2(root->left);
    isBSTReturn right = isBST2(root->right);
    int minimum = min(root->data, min(left.minimum, right.minimum));
    int maximum = max(root->data, max(left.maximum, right.maximum));
    bool isBSTFinal = (root->data > left.maximum) && (root->data <= right.minimum) && left.isBST && right.isBST;

    isBSTReturn output;
    output.minimum = minimum;
    output.maximum = maximum;
    output.isBST = isBSTFinal;

    return output;
}

bool checkBST(BinaryTreeNode<int>* root){
    if(root == NULL){
        return true;
    }
    return isBST2(root).isBST;
}


// Check if tree is BST or not most easiest and most efficient solution
bool checkBST_better(BinaryTreeNode<int>* root, int min = INT_MIN, int max = INT_MAX){
    if(root == NULL){
        return true;
    }
    if(root->data < min || root->data > max){
        return false;
    }

    bool isLeftOK = checkBST_better(root->left, min, root->data - 1);
    bool isRightOK = checkBST_better(root->right, root->data, max);

    return isLeftOK && isRightOK;
}


// Get the path from root to node
vector<int>* getRootToNodePath(BinaryTreeNode<int>* root, int data){
    if(root == NULL){
        return NULL;
    }

    if(root->data == data){
        vector<int>* output = new vector<int>();
        output->push_back(root->data);
        return output;
    }

    vector<int>* leftOutput = getRootToNodePath(root->left, data);
    if(leftOutput != NULL){
        leftOutput->push_back(root->data);
        return leftOutput;
    }
    vector<int>* rightOutput = getRootToNodePath(root->right, data);
    if(rightOutput != NULL){
        rightOutput->push_back(root->data);
        return rightOutput;
    }
    else{
        return NULL;
    }
} 



// Tree: 1 2 3 4 5 6 7 -1 -1 -1 -1 8 9 -1 -1 -1 -1 -1 -1
// InOrder : 4 2 5 1 8 6 9 3 7
// PreOrder : 1 2 4 5 3 6 8 9 7

int main(){
    // BinaryTreeNode<int>* root = new BinaryTreeNode<int>(1);
    // BinaryTreeNode<int>* node1 = new BinaryTreeNode<int>(2);
    // BinaryTreeNode<int>* node2 = new BinaryTreeNode<int>(3);
    // root->left = node1;
    // root->right = node2;

    // BinaryTreeNode<int>* root = takeInput();
    
    BinaryTreeNode<int>* root = level_Wise_input();
    printTree(root);
    cout<<"\nNumber of nodes: "<<numNodes(root)<<endl;
    inOrderTraversal(root);
    cout<<endl;
    preOrderTraversal(root);
    cout<<endl;
    pair<int,int> p = heightDiameter(root);
    cout<<"Height of the Binary Tree: "<<p.first<<endl;
    cout<<"Diameter of the Binary Tree: "<<p.second<<endl;
    cout<<"Sum of all nodes: "<<sumOfAllNodes(root)<<endl;
    bool val = checkBST_better(root);
    if(val){
        cout<<"True"<<endl;
    }
    else{
        cout<<"False"<<endl;
    }

    vector<int>* output = getRootToNodePath(root, 8);
    for(int i=0;i<output->size();i++){
        cout<<output->at(i)<<" ";
    }
    cout<<endl;
    delete output;
    delete root;
    
    /*int inOrder[9] = {4, 2, 5, 1, 8, 6, 9, 3, 7};
    int preOrder[9] = {1, 2, 4, 5, 3, 6, 8, 9, 7};
    BinaryTreeNode<int>* root = buildTree(inOrder, preOrder, 9);
    printTree(root);*/
}