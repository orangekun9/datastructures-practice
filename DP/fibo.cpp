// Using Memoization technique, prev time comlexity = O(2^n), now its O(n)

#include<iostream>
using namespace std;

int fibonac(int *ans, int n){
    if(n <= 1){
        return n;
    }

    // Check if output already exits
    if(ans[n] != -1){
        return ans[n];
    }

    int a = fibonac(ans, n-1);
    int b = fibonac(ans, n-2);
    
    // Save the output for future use
    ans[n] = a + b;

    // Returning the final output
    return ans[n];
}

int fibonac(int n){
    int* ans = new int[n+1];
    for(int i=0;i<=n;i++){
        ans[i] = -1;
    }
    fibonac(ans, n);
    ans[0] = 0;
    ans[1] = 1;
    for(int i=0;i<=n;i++){
        cout<<ans[i]<<" ";
    }
    return ans[n];
}

int main(){
    int n;
    cin>>n;
    fibonac(n);
    cout<<endl;
}