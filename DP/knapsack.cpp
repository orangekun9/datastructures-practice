#include<iostream>
using namespace std;

int knapsack(int* weights, int* values, int n, int maxWeight){
    if(n == 0 || maxWeight == 0){
        return 0;
    }

    if(weights[0] > maxWeight){
        return knapsack(weights + 1, values + 1, n - 1, maxWeight);
    }
    
    int a = knapsack(weights + 1, values + 1, n - 1, maxWeight-weights[0]) + values[0];
    int b = knapsack(weights + 1, values + 1, n - 1, maxWeight);

    return max(a,b);
}

int main(){
    int weights[] = {10,3,2,5,20};
    int values[] = {5,7,1,0,8}; 

    cout<<knapsack(weights, values, 5, 16)<<endl;
}