#include<iostream>
using namespace std;

int lcs_memo(string s, string t, int** output){
    // Base case
    if(s.size()==0 || t.size()==0){
        return 0;
    }

    // Check if answer is already exists
    if(output[s.size()][t.size()] != -1){
        return output[s.size()][t.size()];
    }

    int ans;
    if(s[0] == t[0]){
        ans = 1 + lcs_memo(s.substr(1), t.substr(1), output);
    }
    else{
        int a = lcs_memo(s.substr(1), t, output);
        int b = lcs_memo(s, t.substr(1), output);
        int c = lcs_memo(s.substr(1), t.substr(1), output);

        ans = max(a, max(b,c));
    }

    // Save your calculation
    output[s.size()][t.size()] = ans;

    return ans;
}

int lcs(string s, string t){
    int m = s.size();
    int n = t.size();
    int** output = new int*[m+1];

    for(int i=0;i<=m;i++){
        output[i] = new int[n+1];
        for(int j=0;j<=n;j++){
            output[i][j] = -1;
        }
    }

    return lcs_memo(s, t, output);
}

int main(){
    string s, t;
    cin>>s>>t;
    cout<<lcs(s,t)<<endl;
}