#include<iostream>
#include<vector>
#include<climits>
using namespace std;

int minCostSum(int **input, int n, int m, int i, int j){
    // Base case
    if(i==n-1 && j==m-1){
        return input[i][j];
    }

    if(i>=n || j>=m){
        return INT_MAX;
    }

    // Recursion call
    int x = minCostSum(input, n, m, i, j+1);
    int y = minCostSum(input, n, m, i+1, j+1);
    int z = minCostSum(input, n, m, i+1, j);

    // Small Calculation
    int ans = min(x, min(y,z)) + input[i][j];

    return ans;
}

int minCostSum(int **input, int n, int m){
    return minCostSum(input, n, m, 0, 0);
}

int main(){
    int n, m;
    cin>>n>>m;
    int **input = new int*[n];

    for(int i=0;i<n;i++){
        input[i] = new int[m];
        for(int j=0;j<m;j++){
            cin>>input[i][j];
        } 
    }

    cout<<minCostSum(input, n, m);
}