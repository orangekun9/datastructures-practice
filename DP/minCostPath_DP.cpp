#include<iostream>
using namespace std;

int main(){
    int m,n;
    cin>>m>>n;
    int **input = new int*[m];

    for(int i=0;i<m;i++){
        input[i] = new int[n];
        for(int j=0;j<n;j++){
            cin>>input[i][j];
        }
    }


    int **output = new int*[m];
    // Creating an output array with garbage values;
    for(int i=0;i<m;i++){
        output[i] = new int[n];
    }

    // Fill the last cell , i.e. destination
    output[m-1][n-1] = input[m-1][n-1];

    // Fill row (right to left)
    for(int j=n-2;j>=0;j--){
        output[m-1][j] = output[m-1][j+1] + input[m-1][j];
    }

    // Fill last column(bottom to top)
    for(int i=m-2;i>=0;i--){
        output[i][n-1] = output[i+1][n-1] + input[i][n-1];
    }

    // Fill remaining cells
    for(int i=m-2;i>=0;i--){
        for(int j=n-2;j>=0;j--){
            output[i][j] = min(output[i][j+1],min(output[i+1][j+1], output[i+1][j])) + input[i][j];
        }
    }


    cout<<output[0][0];

}