#include<iostream>
#include<vector>
#include<climits>
using namespace std;

int minCostSum(int **input, int n, int m, int i, int j, int **output){
    // Base case
    if(i==n-1 && j==m-1){
        return input[i][j];
    }

    if(i>=n || j>=m){
        return INT_MAX;
    }

    if(output[i][j] != -1){
        return output[i][j];
    }

    // Recursion call
    int x = minCostSum(input, n, m, i, j+1, output);
    int y = minCostSum(input, n, m, i+1, j+1, output);
    int z = minCostSum(input, n, m, i+1, j, output);

    // Small Calculation
    int ans = min(x, min(y,z)) + input[i][j];

    output[i][j] = ans;

    return ans;
}

int minCostSum(int **input, int n, int m){

    int **output = new int*[n];

    for(int i=0;i<n;i++){
        output[i] = new int[m];
        for(int j=0;j<m;j++){
            output[i][j] = -1;
        } 
    }

    minCostSum(input, n, m, 0, 0, output);

    return output[0][0];
}

int main(){
    int n, m;
    cin>>n>>m;
    int **input = new int*[n];

    for(int i=0;i<n;i++){
        input[i] = new int[m];
        for(int j=0;j<m;j++){
            cin>>input[i][j];
        } 
    }

    cout<<minCostSum(input, n, m);
}