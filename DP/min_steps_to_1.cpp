#include<iostream>
#include<climits>
using namespace std;

int minSteps_memoization_helper(int *arr, int n){
    if(n<=1){
        return 0;
    }

    if(arr[n] != -1){
        return arr[n];
    }

    int x = minSteps_memoization_helper(arr, n-1);

    int y = INT_MAX, z = INT_MAX;
    if(n%2 == 0){
        y = minSteps_memoization_helper(arr, n/2);
    }
    
    if(n%3 == 0){
        z = minSteps_memoization_helper(arr, n/3);
    }

    arr[n] = min(x, min(y,z)) + 1;
    return arr[n];
}

int minSteps_memoization(int n){
    int* arr = new int[n+1];
    for(int i=0;i<=n;i++){
        arr[i] = -1;
    }
    minSteps_memoization_helper(arr, n);
    return arr[n];
}


int minSteps(int n){
    // Base case
    if(n <= 1){
        return 0;
    }

    int x = minSteps(n-1);

    int y = INT_MAX, z = INT_MAX;
    if(n%2 == 0){
        y = minSteps(n/2);
    }
    
    if(n%3 == 0){
        z = minSteps(n/3);
    }

    return min(x, min(y,z)) + 1;
}

int main(){
    int n;
    cin>>n;
    cout<<minSteps_memoization(n)<<endl;
}