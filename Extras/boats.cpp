#include <iostream>
#include <sstream>
using namespace std;

class Solution
{
public:
    int numBoats(int arr[100][100], int row, int col)
    {

        pair<int, bool> newArr[100][100];
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                newArr[i][j] = make_pair(arr[i][j], false);
            }
        }

        int count = 0;
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                if (newArr[i][j].second == false)
                {
                    if (j + 1 < col)
                    {
                        if ((newArr[i][j].first == 1) && (newArr[i][j + 1].first == 1 && newArr[i][j + 1].second == false))
                        {
                            newArr[i][j + 1].second = true;
                            count++;
                        }
                    }

                    if (i + 1 < row)
                    {
                        if ((newArr[i][j].first == 1) && (newArr[i + 1][j].first == 1 && newArr[i + 1][j].second == false))
                        {
                            newArr[i + 1][j].second = true;
                            count++;
                        }
                    }

                    if (i + 1 < row && j + 1 < col)
                    {
                        if ((newArr[i][j].first == 1) && (newArr[i + 1][j + 1].first == 1 && newArr[i + 1][j + 1].second == false))
                        {
                            newArr[i + 1][j + 1].second = true;
                            count++;
                        }
                    }
                    newArr[i][j].second == true;
                }
            }
        }
        return count;
    }
};

int main()
{
    int row, col, i, j, k;
    cin >> row;
    cin >> col;
    int arr[100][100];

    for (i = 0; i < row; i++)
    {
        for (j = 0; j < col; j++)
        {
            cin >> k;
            arr[i][j] = k;
        }
    }
    int ret = Solution().numBoats(arr, row, col);
    cout << ret << endl;
    return 0;
}