#include <iostream>
#include <algorith>
using namespace std;

class Edge
{
    int source;
    int destination;
    int weight;
};

bool compare(Edge e1, Edge e2)
{
    return e1.weight < e2.weight;
}

int getParent(int *parent, int data)
{
    while (parent[data] != data)
    {
        data = parent[data];
    }
    return data;
}

int main()
{
    int n, E;
    cin >> n >> E;

    Edge *input = new Edge[E];

    for (int i = 0; i < E; i++)
    {
        int s, d, w;
        cin >> s >> d >> w;
        input[i].source = s;
        input[i].destination = d;
        input[i].weight = w;
    }

    sort(input, input + E, compare);

    Edge *output = new Edge[n - 1];
    int count = 0;
    int i = 0;

    int parent[n];
    for (int i = 0; i < n; i++)
    {
        parent[i] = i;
    }

    while (count != n - 1)
    {
        parent[input[i].source] = getParent(parent, input[i].source);
        parent[input[i].destination] = getParent(parent, input[i].destination);

        if (parent[input[i].source] != parent[input[i].destination])
        {
            output[count] = input[i];
            count++;
            parent[input[i].destination] = parent[input[i].source];
        }
        i++;
    }

    for (int i = 0; i < n - 1; i++)
    {
        if (output[i].source <= output[i].destination)
        {
            cout << output[i].source << " " << output[i].destination << " " << output[i].weight << endl;
        }
        else
        {
            cout << output[i].destination << " " << output[i].source << " " << output[i].weight << endl;
        }
    }
}