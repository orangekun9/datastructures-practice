#include <iostream>
#include <queue>
#include <unordered_map>
using namespace std;

// Depth First Search
void printDFS(int **edges, int n, int sv, bool *visited)
{
    cout << sv << " ";
    visited[sv] = true;

    for (int i = 0; i < n; i++)
    {
        if (i == sv)
        {
            continue;
        }
        if (edges[sv][i] == 1)
        {
            if (visited[i] == true)
            {
                continue;
            }
            printDFS(edges, n, i, visited);
        }
    }
}

// BFS
void printBFS(int **edges, int n, int sv, bool *visited)
{
    queue<int> pendingVertices;
    pendingVertices.push(sv);
    visited[sv] = true;

    while (!pendingVertices.empty())
    {
        int currentVertex = pendingVertices.front();
        pendingVertices.pop();
        cout << currentVertex << " ";

        for (int i = 0; i < n; i++)
        {
            if (edges[currentVertex][i] == 1 && !visited[i])
            {
                pendingVertices.push(i);
                visited[i] = true;
            }
        }
    }
}

// For Disconnect Graphs
void BFS(int **edges, int n)
{
    bool *visited = new bool[n];
    for (int i = 0; i < n; i++)
    {
        visited[i] = false;
    }

    for (int i = 0; i < n; i++)
    {
        if (!visited[i])
        {
            printBFS(edges, n, i, visited);
        }
    }
    delete[] visited;
}

void DFS(int **edges, int n)
{
    bool *visited = new bool[n];
    for (int i = 0; i < n; i++)
    {
        visited[i] = false;
    }

    for (int i = 0; i < n; i++)
    {
        if (!visited[i])
        {
            printDFS(edges, n, i, visited);
        }
    }

    delete[] visited;
}

// Get Path DFS
vector<int> getPath_DFS(int **edges, int v, int sv, int ev, bool *visited)
{
    if (sv == ev)
    {
        vector<int> ans;
        ans.push_back(sv);
        return ans;
    }

    vector<int> temp;
    visited[sv] = true;
    for (int i = 0; i < v; i++)
    {
        if (edges[sv][i] == 1 && !visited[i])
        {
            vector<int> val = getPath_DFS(edges, v, i, ev, visited);
            if (val.size() != 0)
            {
                temp = val;
                break;
            }
        }
    }

    if (temp.size() != 0)
    {
        temp.push_back(sv);
    }

    return temp;
}

// Get Path BFS Shortest Path
void getPath_BFS(int **edges, int v, int sv, int ev, bool *visited)
{
    queue<int> pendingNodes;
    pendingNodes.push(sv);
    visited[sv] = true;
    unordered_map<int, int> map;

    while (pendingNodes.size() != 0)
    {
        int currentVertex = pendingNodes.front();
        pendingNodes.pop();

        for (int i = 0; i < v; i++)
        {
            if (edges[sv][i] == 1 && visited[i] == false)
            {
                pendingNodes.push(i);
                visited[i] = true;
                map[i] = currentVertex;
                if (i == ev)
                {
                    break;
                }
            }
        }
    }

    cout << ev << " ";
    while (ev != sv)
    {
        cout << map[ev] << " ";
        ev = map[ev];
    }
}

int main()
{
    int n, e, firstVal, secondVal;
    cin >> n >> e;
    int **edges = new int *[n];

    for (int i = 0; i < n; i++)
    {
        edges[i] = new int[n];
        for (int j = 0; j < n; j++)
        {
            edges[i][j] = 0;
        }
    }

    for (int i = 0; i < e; i++)
    {
        int f, s;
        cin >> f >> s;
        edges[f][s] = 1;
        edges[s][f] = 1;
    }

    bool *visited = new bool[n];
    for (int i = 0; i < n; i++)
    {
        visited[i] = false;
    }

    cout << "DFS: ";
    DFS(edges, n);
    cout << endl;
    cout << "BFS: ";
    BFS(edges, n);
    cout << endl;

    cin >> firstVal >> secondVal;
    cout << "DFS Path between " << firstVal << " and " << secondVal << endl;
    vector<int> path = getPath_DFS(edges, n, firstVal, secondVal, visited);

    for (int i = 0; i < path.size(); i++)
    {
        cout << path.at(i) << " ";
    }

    cout << "\nBFS or Shortest Path between " << firstVal << " and " << secondVal << endl;
    getPath_BFS(edges, n, firstVal, secondVal, visited);
    delete[] visited;
    for (int i = 0; i < n; i++)
    {
        delete[] edges[i];
    }
    delete[] edges;
}