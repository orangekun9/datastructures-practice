#include<iostream>
#include<unordered_map>
#include<string>
#include<vector>
using namespace std;

int main(){
    unordered_map<string, int> map;
    map["abc1"] = 1;
    map["abc2"] = 2;
    map["abc3"] = 3;
    map["abc4"] = 4;
    map["abc5"] = 5;
    map["abc6"] = 6;

    // Defining iterators
    unordered_map<string, int>::iterator it = map.begin();
    while(it != map.end()){
        // Since "it" is pointing towarard memory block of the map
        cout<<it->first<<" "<<it->second<<endl;
        it++;
    }

    // search also uses iterators
    unordered_map<string, int>::iterator it2 = map.find("abc");

    // Another thing other than iterators
    vector<int> v;
    v.push_back(1);
    v.push_back(12);
    v.push_back(13);
    v.push_back(14);
    v.push_back(15);
    v.push_back(16);
    //  erase
    vector<int>::iterator it3 = v.begin();
    v.erase(it3, it3+4);//start + start + 4 i.e for {1 2 3 4 5 6} output will be {5 6}

    while(it3 != v.end()){
        cout<<*it3<<endl;
        it3++;
    }
}
