#include<vector>
#include<unordered_map>
using namespace std;
vector<int> longestSubsequence(int *arr, int n){
	// Write your code here
    unordered_map<int,int> a,b;
    int maxLength=-1,key;
    for(int i=0;i<n;i++)
    {
        if(b.find(arr[i])==b.end())
        {
            b[arr[i]]=i;
            a[arr[i]]=true;
        }
    }
    for(int i=0;i<n;i++)
    {        
        if(a[arr[i]])
        {            
            a[arr[i]]=false;
            int k=1,count=1;
            while(a[arr[i]+k])
            { 
                a[arr[i]+k]=false;
                count++;
                k++;
            }
            k=-1;
            while(a[arr[i]+k])
            { 
                a[arr[i]+k]=false;
                count++;
                k--;
            }    
            if(count>maxLength)
            {
                maxLength=count;
                key=arr[i]+k+1;
            }
            else if(count==maxLength&&b[arr[i]+k+1]<b[key])
            {
                maxLength=count;
                key=arr[i]+k+1;
            }
            
           // cout<<arr[i]<<" "<<count<<" "<<arr[i]+k+1<<endl;
        }
                
    }
    
    vector<int>* v=new vector<int>;
    for(int i=0;i<maxLength;i++)
    {
        v->push_back(key+i);
    }
    return *v;
}