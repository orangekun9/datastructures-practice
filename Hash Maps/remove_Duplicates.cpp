#include<iostream>
#include<unordered_map>
#include<vector>
using namespace std;

vector<int> removeDuplicates(int output[], int size){
    unordered_map<int, bool> mymap;
    vector<int> res;
    for(int i=0;i<size;i++){
        if(mymap.count(output[i])>0){
            continue;
        }
        mymap[output[i]] = true;
        res.push_back(output[i]);
    }

    return res;
}

int main(){
    int output[] = {1,2,3,3,2,1,4,3,6,5,5};
    vector<int> data = removeDuplicates(output, 11);

    for(int i=0;i<data.size();i++){
        cout<<data.at(i)<<" ";
    }
    cout<<endl;
}