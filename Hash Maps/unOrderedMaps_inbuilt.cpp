#include<iostream>
#include<unordered_map>
#include<string>
using namespace std;

int main(){
    unordered_map<string, int> myMap;

    // Insert
    pair<string, int> p("abc", 1);
    myMap.insert(p);

    // Another easy way for inertion in unordered maps
    myMap["abc"] = 1;

    // find or access
    cout<<myMap["abc"]<<endl;  // if "abc" is not a key in the current map then this map object will create key "abc" and assign default value i.e.. 0
    cout<<myMap.at("abc")<<endl;  // if "abc" is not present in the current map then .at() method will going to return an exception error

    // Check Presence
    if(myMap.count("abc") > 0){
        cout<<"abc is present"<<endl;
    }

    // Size of the map
    cout<<myMap.size()<<endl;

    // Erase
    myMap.erase("abc");
    cout<<myMap.size()<<endl;
}