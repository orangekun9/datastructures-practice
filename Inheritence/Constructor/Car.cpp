#include "Vehicle.cpp"

class Car : public Vehicle
{
public:
    // Car()
    // {
    //     cout << "Car's constructor" << endl;
    // }

    Car(int a, int b) : Vehicle(a)
    {
        cout << "Car's parameterized constructor" << endl;
    }

    ~Car()
    {
        cout << "Car's destructor" << endl;
    }
};