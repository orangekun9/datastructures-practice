class Vehicle
{
public:
    // Vehicle()
    // {
    //     cout << "Vehicle's constructor" << endl;
    // }

    Vehicle(int x)
    {
        cout << "Vehicle's parameterized constructor" << endl;
    }

    ~Vehicle()
    {
        cout << "Vehicle's destructor" << endl;
    }
};