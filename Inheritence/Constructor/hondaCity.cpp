#include "Car.cpp"

class hondaCity : public Car
{
public:
    // hondaCity() : Car(5)
    // {
    //     cout << "Honda City constructor" << endl;
    // }

    hondaCity(int x, int y) : Car(x, y)
    {
        cout << "Honda City parameterized constructor" << endl;
    }

    ~hondaCity()
    {
        cout << "Honda City destructor" << endl;
    }
};