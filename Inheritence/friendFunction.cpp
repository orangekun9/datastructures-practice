#include <iostream>
using namespace std;

class Bus
{
public:
    void print();
};

class Truck
{
private:
    int x;

protected:
    int y;

public:
    int z;

    // friend void Bus::print();
    friend class Bus; // Shortcut
};

void Bus::print()
{
    Truck v;
    v.x = 1;
    v.y = 2;
    v.z = 3;
    cout << v.x << " " << v.y << " " << v.z << endl;
}

int main()
{
    Bus b;
    b.print();
}
