#include<iostream>
#include "Node.cpp"
using namespace std;


// THe best possible approach for reversing the linked list without using any extra class with O(n) time complexity (recursion)
Node* reverse2(Node* head){
    if(head == NULL || head->next == NULL){
        return head;
    }
    Node* smallAns = reverse2(head->next);
    Node* tail = head->next;
    tail->next = head;
    head->next = NULL;
    return smallAns;

}


// For reversing a linked list
class Pair{
    public:
        Node* head;
        Node* tail;
};

// Reversing a linked list with time complexity of O(n)
Pair reverse(Node* head){
    if(head == NULL || head->next == NULL){
        Pair ans;
        ans.head = head;
        ans.tail = head;
        return ans;
    }
    
    Pair smallAns = reverse(head->next);

    smallAns.tail->next = head;
    head->next = NULL;

    Pair ans;
    ans.head = smallAns.head;
    ans.tail = head;
    
    return ans;
}

Node* reverseLL_better(Node* head){
    return reverse(head).head;
}


// This function has time complexity of O(n*n);
Node* takeInput(){
    int data;
    cin>>data;
    Node *head = NULL;
    while(data != -1){
        Node* newNode = new Node(data);
        if(head == NULL){
            head = newNode;
        }
        else{
            Node *temp = head;
            while(temp->next != NULL){
                temp = temp->next;
            }
            temp->next = newNode;
        }
        cin>>data;
    }
    return head;
}

// This function has time complexity of O(n)
Node *takeInput_Better(){
    int data;
    Node* head = NULL;
    Node* tail = NULL;
    cin>>data;
    while(data != -1){
        Node* newNode = new Node(data);
        if(head == NULL){
            head = newNode;
            tail = head;
        }
        else{
            tail->next = newNode;
            tail = newNode;
        }
        cin>>data;
    } 
    return head;   
}


Node *insertNode(Node* head, int i, int data){
    Node* newNode = new Node(data);
    Node* temp = head;

    if( i == 0){
        newNode->next = head;
        head = newNode;
        return head;
    }
    int count = 0;
    while( temp!=NULL && count < i-1 ){
        temp = temp->next;
        count++;
    }
    if(temp != NULL){
        newNode->next = temp->next;
        temp->next = newNode;
    }
    return head;
}

void print(Node *head){
    Node *temp = head;
    while(temp != NULL){
        cout<<temp->data<<" ";
        temp = temp->next;
    }
    cout<<endl;
}

// Reverse a linked list 
// But this approach is shit because time complexity is O(n*n)
Node* reverseLL(Node* head){
    if(head == NULL || head->next == NULL){
        return head;
    }

    Node* smallAns = reverseLL(head->next);
    Node* temp = smallAns;
    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = head;
    head->next = NULL;
    return smallAns;
}
void delete_alternate_node_LinkedList(Node *head) {
    /* Don't write main().
     * Don't read input, it is passed as function argument.
     * Change in the given input itself.
     * No need to return pr print the output.
     * Taking input and printing output is handled automatically.
     */
    if(head == NULL){
        return;
    }
    
    Node* current = head;
    
    while(current != NULL || current->next != NULL){
        Node* a = current->next;
        current->next = a->next;
        if(current->next == NULL){
            break;
        }
        current = current->next;
        delete a;
    }

    while(head!=NULL){
        cout<<head->data<<" ";
    }
}
int main(){
    // Statically
    //Node n1(10);
    //Node *head = &n1;

    //Node n2(20);
    //n1.next = &n2;

    //cout<<n1.data<<" "<<n2.data<<endl;

    // Dynamically
    //Node *n3 = new Node(30);
    //Node *headd = n3;

    //Node *n4 = new Node(40);
    //n3->next = n4;

    //print(head);
    //print(headd);

    Node *head = takeInput_Better();
    // print(head);
    // int i,val;
    // cin>>i>>val;
    // head = insertNode(head, i, val);
    // print(head); 
    // head = reverseLL_better(head);
    //head = reverse2(head);
    delete_alternate_node_LinkedList(head);
    //print(head);

}