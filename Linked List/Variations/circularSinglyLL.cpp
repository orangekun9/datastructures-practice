#include<iostream>
using namespace std;

class node{
    public:
        int data;
        node* next = NULL;

        node(int data){
            this->data = data;
        }
};

// Inserting a node
node* insert(){
    int data;
    cin>>data;
    node* head = NULL;
    while(data != -1){
        node* newNode = new node(data);
        if(head == NULL){
            head = newNode;
        }
        else if(head->next == NULL){
            head->next = newNode;
            newNode->next = head;
        }
        else{
            node* temp = head;
            while(temp->next != head){
                temp = temp->next;
            }
            newNode->next = head;
            temp->next = newNode;
        }
        cin>>data;
    }
    return head;
}

// Deleting a node
node* deleteLL(node *head, int i){
    node* temp = head;
    while(temp->next != head){
        temp = temp->next;
    }
    if(i==0){
        node* curr = head;
        head = head->next;
        temp->next = head;
        delete curr;
        return head;
    }
    int len = 1;
    node* t1 = head;
    while(len<i){
        t1 = t1->next;
        len++;
    }
    node* curr = t1->next;
    t1->next = t1->next->next;
    delete curr;
    
    return head;
}

void print(node* head){
    node* temp = head;
    while(temp->next != head){
        cout<<temp->data<<" ";
        temp = temp->next;
    }
    cout<<temp->data<<endl;
}

int main(){
    node *head = insert();
    print(head);
    int n;
    cin>>n;
    head = deleteLL(head,n);
    print(head);
    
}