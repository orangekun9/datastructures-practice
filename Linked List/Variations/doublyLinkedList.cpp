#include<iostream>
using namespace std;

class node{
    public:
        int data;
        node* next;
        node* prev;

        node(int data){
            this->data = data;
            next = NULL;
            prev = NULL;
        }
};

// Adding a node
node* addingNode(){
    node* head = NULL;
    node* tail = NULL;
    int data;
    cin>>data;
    while(data != -1){
        node* newNode = new node(data);
        if(head == NULL){
            head = newNode;
            tail = head;
        }
        else{
            tail->next = newNode;
            newNode->prev = tail;
            tail = newNode;
        }
        cin>>data;
    }
    return head;
}

// Deleting a node
node* deleteLL(node* head, int i){
    if(i==0){
        node* curr = head;
        head = head->next;
        delete curr;
        return head;
    }
    else{
        node* temp = head;
        int len = 1;
        while(len < i){
            temp = temp->next;
            len++;
        }
        node* curr = temp->next;
        temp->next = temp->next->next;
        delete curr;
    }
    return head;
}


void print(node* head){
    node* temp = head;
    while(temp!=NULL){
        cout<<temp->data<<" ";
        temp = temp->next;
    }
    cout<<endl;
}

int main(){
    node* head = addingNode();
    print(head);
    int n;
    cin>>n;
    head = deleteLL(head,n);
    print(head);
}