#include<iostream>
#include "student.cpp"
using namespace std;

int main(){
    // Creating objects statically
    Student s1;
    Student s2;
    Student s3,s4,s5;

    s1.rollNumber = 24;
    s1.age = 10;

    cout<<s1.age<<endl;
    cout<<s1.rollNumber<<endl;

    // Create objects dynamically
    Student* s6 = new Student();
    (*s6).age = 24;
    (*s6).rollNumber = 1;
    // Or
    s6->age = 100;
    s6->rollNumber = 100;
    cout<<(*s6).age<<endl;
    cout<<(*s6).rollNumber<<endl;
}