#include<iostream>
using namespace std;

class Array{
    int nextIndex, capacity;
    int *arr;

    public:
        Array(){
            arr = new int[5];
            nextIndex = 0;
            capacity = 5;
        }

        // Overriding Copy Constructor
        Array( Array const & a){
            //this->arr = a;  // Shallow Copy
            
            // Deep Copy
            this->capacity = a.capacity;
            this->nextIndex = a.nextIndex;
            this->arr = new int[this->capacity];
            for(int i=0;i<a.nextIndex;i++){
                this->arr[i] = a.arr[i];
            }
        }

        // Overriding = Operator which is used for Copy assignment operator
        void operator=( Array const & a){
            // Deep Copy
            this->capacity = a.capacity;
            this->nextIndex = a.nextIndex;
            this->arr = new int[this->capacity];
            for(int i=0;i<a.nextIndex;i++){
                this->arr[i] = a.arr[i];
            }
        }

        void add(int n){
            if(nextIndex == capacity){
                int *newArr = new int[2*capacity];
                for(int i=0;i<capacity;i++){
                    newArr[i] = arr[i];
                }
                delete [] arr;
                arr = newArr;
                capacity *= 2;
            }
            arr[nextIndex++] = n;
        }

        int get(int n) const{
            if(n < nextIndex){
                return arr[n];
            }
            else{
                return -1;
            }
        }

        void addAtPerticularIndex(int element, int i){
            if( i < nextIndex ){
                arr[i] = element;
            }
            else if( i == nextIndex ){
                add(element);
            }
            else{
                return; 
            }
        }

        void print() const{
            for(int i=0;i<nextIndex;i++){
                cout<<arr[i]<<" ";
            }
            cout<<endl;
        }
};