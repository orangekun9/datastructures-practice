#include<iostream>
#include "DynamicArray.cpp"
using namespace std;

int main(){
    Array a1;
    for(int i=1;i<=5;i++){
        a1.add(i);
    }

    a1.print();
    a1.addAtPerticularIndex(12, 5);
    a1.print();
    cout<<a1.get(2)<<endl;

    Array a2(a1);
    Array a3 = a2;
    if(a1 == a2){
        return true;
    }
    a2.print();
    a3.print();
}