#include<iostream>
using namespace std;

class Fraction{
    private:
        int numerator;
        int denominator;
    
    public:
        Fraction(int numerator, int denominator){
            this->numerator = numerator;
            this->denominator = denominator;
        }

        void print(){
            cout<<numerator<<"/"<<denominator<<endl;
        }

        void simplify(){
            int gcd = 1;
            int j = min( this->numerator, this->denominator ); // Givs the minimum between 2 given arguments
            for(int i=1;i<=j;i++){
                if(this->numerator%i == 0 && this->denominator%i == 0){
                    gcd = i;
                }
            }

            this->numerator = this->numerator/gcd;
            this->denominator = this->denominator/gcd;
        }

        void mul( Fraction const &f2){
            denominator = denominator * f2.denominator;
            numerator = numerator * f2.numerator;

            simplify();
        }

        void add( Fraction const &f2 ){
            int lcm = denominator * f2.denominator;
            int x = lcm/denominator;
            int y = lcm/f2.denominator;
            
            int num = x*numerator + (y*f2.numerator);
            
            numerator = num;
            denominator = lcm;

            simplify(); // Simplyifying the fraction
        }
};