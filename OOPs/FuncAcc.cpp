#include<iostream>
#include "Fraction.cpp"
using namespace std;

int main(){
    Fraction f1(10,2);
    f1.print();
    
    Fraction f2(15,4);
    f2.print();

    cout<<"After Addition: ";
    f1.add(f2);
    f1.print();

    cout<<"After Multiplication: ";
    f1.mul(f2);
    f1.print();
}