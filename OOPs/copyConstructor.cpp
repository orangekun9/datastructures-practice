#include<iostream>
#include "student.cpp"
using namespace std;

int main(){
    Student* s1 = new Student(12,20);
    cout<<"S1: ";
    s1->display();

    // Using copy constructor
    Student* s2 = new Student(*s1); // Copying the elements of object s1 to object s2
    cout<<"S2: ";
    s2->display();

    // Using copy assignment operator
    Student* a = new Student(10,10);
    Student* b = new Student(20,20);
    cout<<"A: ";
    a->display();
    cout<<"B: ";
    b->display();

    *b = *a;
    cout<<"After using assignment operator"<<endl;
    cout<<"B: ";
    b->display();

    delete s1;
    delete s2;
    delete a;
    delete b;
}