#include<iostream>
#include "shallow.cpp"
using namespace std;

int main(){
    char name[] = "abcd";
    Student s1(18, name);
    s1.print();

    name[3] = 'e';
    Student s2(20, name);
    s2.print();
    s1.print();

    // Normal Copy Constructor works do Shallow Copying but this is an override constructor with deep copy implementation
    Student s3(22,name);
    Student s4(s3);
    s4.name[3] = 'x';
    s3.print();
    s4.print();
}