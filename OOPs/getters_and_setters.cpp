// Getters and Setters are basically use to get and set a private value in class

#include<iostream>
#include "student.cpp"
using namespace std;

int main(){
    Student* s1 = new Student;
    // For parameterized constructor
    Student* s2 = new Student(122);
    s1->setAge(18);
    s1->display();

    cout<<s2<<endl;
    s2->setAge(20);
    s2->display();
}