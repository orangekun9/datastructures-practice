#include<iostream>
using namespace std;

class Student{
    public:
        int age;
        const int rollNumber;

        // Using Initialization List
        Student(int a, int roll) : age(a), rollNumber(roll){

        } 
};

int main(){
    Student s1(20,22);
    cout<<s1.age<<" "<<s1.rollNumber<<endl;
}