#include<iostream>
using namespace std;

class Value{
    public:
        int a;

    Value(int a){
        this->a = a;
    }

    // Operator Overloading
    Value operator+( Value const & v) const{
        int m = this->a + v.a;
        Value v3(m);

        return v3;
    }
    // Overload Preincrement
    Value& operator++(){     // Passing as a reference to prevent creating another block for the incrementation of current value
        a = a + 1;
        return *this;
    }

    // Overload Postincrement
    Value operator++(int){  // int parameter is just for differentiation between pre and post increment
        Value newV(a);
        a = a + 1;
        return newV;
    }

    // Overloading += operator
    Value& operator+=(Value const &nV){
        a = a + nV.a;
        return *this;
    }

    void print(){
        cout<<a<<endl;
    }
};

int main(){
    Value v1(10);
    Value v2(30);
    Value v3 = v1 + v2;
    Value v4 = ++(++v1);
    Value v5 = (v2++)++;
    //v1 += v3;
    (v1+=v3) += v4;
    v1.print();
    v2.print();
    v3.print();
    v4.print();
    v1.print();
    v5.print();
    v2.print();
}