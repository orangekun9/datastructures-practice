#include<iostream>
using namespace std;

class Student{
    static int totalStudent; //Declaring static variable

    public:
        int age;
        int rollNumber;

        Student(){
            totalStudent++;
        }

        static int getStudent(){
            return totalStudent;
        }
};

int Student::totalStudent = 0; //Initializing the declared static variable

int main(){
    Student s1;
    Student s2;
    cout<<Student::getStudent()<<endl;
}