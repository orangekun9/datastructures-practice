#include<iostream>
using namespace std;
class Student{
    public:
    int rollNumber;
    int age;

    // Destructor
    ~Student(){
        cout<<"Destructor Called !"<<endl;
    }

    // Default Constructor
    Student(){
        cout<<"Constructor Called !"<<endl;
    }

    // Parametrized Constructor
    Student(int rollNumber){
        cout<<"Constructor 2 Called !"<<endl;
        cout<<"This: "<<this<<endl;
        this->rollNumber = rollNumber;        // This keyword holds the address of the current object.
    }

    // 2nd Parameterized Constructor
    Student(int rol, int a){
        cout<<"Constructor 3 Called !"<<endl;
        rollNumber = rol;
        age = a;
    }
    public:
    void display(){
        cout<<age<<" "<<rollNumber<<"\n";
    }

    int getAge(){
        return age;
    }

    void setAge(int a){
        age = a;
    }
};