#include<iostream>
using namespace std;

int main(){
    string s,t;
    cin>>s>>t;
    
    int m = s.size();
    int n = t.size();

    int **output = new int*[m+1];
    
    // Creating array
    for(int i=0;i<=m;i++){
        output[i] = new int[n+1];
    }

    // Filling 1st row
    for(int j=0;j<=n;j++){
        output[0][j] = 0;
    }

    // Filling 1st col
    for(int i=1;i<=m;i++){
        output[i][0] = 0;
    }

    for(int i=1;i<=m;i++){
        for(int j=1;j<=n;j++){
            // Checking the first elements (if they match or not)
            if(s[m-i] == t[n-j]){
                output[i][j] = 1 + output[i-1][j-1];
            }
            else{
                output[i][j] = max(output[i-1][j], max(output[i-1][j-1], output[i][j-1]));
            }
        }
    }

    cout<<output[m][n]<<endl;
}