#include<iostream>
using namespace std;

template <typename T>

class Queue{
    T* data;
    int size;
    int frontIndex;
    int nextIndex;
    int capacity=5;

    public:

        Queue(){
            data = new T[capacity];
            size=0;
            frontIndex = -1;
            nextIndex = 0;
        }

        T Size(){
            return size;
        }

        bool isEmpty(){
            return size==0;
        }

        void enqueue(T element){
            if(size == capacity){
                T* newData = new T[2*capacity];
                int j=0;
                for(int i=frontIndex;i<capacity;i++){
                    newData[j] = data[i];
                    j++;
                }

                for(int i=0;i<frontIndex;i++){
                    newData[j] = data[i];
                    j++;
                }

                delete [] data;
                data = newData;
                nextIndex = capacity;
                capacity *= 2;
            }

            data[nextIndex] = element;
            nextIndex = (nextIndex + 1)%capacity;
            if(frontIndex == -1){
                frontIndex = 0;
            }
            size++;
        }


        T front(){
            if(isEmpty()){
                cout<<"Queue is empty"<<endl;
                return 0;
            }
            return data[frontIndex];
        }

        T dequeue(){
            if(isEmpty()){
                cout<<"Queue is empty"<<endl;
                return 0;
            }
            T ans = data[frontIndex];
            frontIndex = (frontIndex + 1)%capacity;
            size--;
            if(size==0){
                frontIndex == -1;
                nextIndex = 0;
            }
            return ans;
        }
};


int main(){
    Queue<int> q;

    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(3);
    q.enqueue(4);
    q.enqueue(5);
    q.enqueue(6);

    cout<<q.Size()<<endl;

    cout<<q.front()<<endl;

    cout<<q.dequeue()<<endl;

    cout<<q.isEmpty()<<endl;

    cout<<q.Size()<<endl;
}