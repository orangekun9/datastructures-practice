#include<iostream>
using namespace std;

int countZ(int n){
    if(n==0){
        return 0;
    }
    if(n%10 == 0){
        return 1+countZ(n/10);
    }
    else{
        return countZ(n/10);
    }
}

int main(){
    int n;
    cin>>n;
    int zero = countZ(n);
    cout<<zero<<endl;
}