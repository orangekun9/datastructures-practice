// Geometric Sum of number k = 1 + 1/2 + 1/4 + 1/8 + ... + 1/(2^k) 
#include<iostream>
#include<cmath>
using namespace std;

double geoSum(int k){
    if(k==0){
        return 1;
    }
    double a = geoSum(k-1);
    double val = a+(1)/(pow(2,k));
    return val;
}

int main(){
    int k;
    cin>>k;
    double ans = geoSum(k);
    cout<<ans<<endl;
}