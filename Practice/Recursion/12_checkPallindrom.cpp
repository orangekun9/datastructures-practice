#include<iostream>
#include<cstring>
using namespace std;

bool checkPalin(char input[], int s, int e){
    if(s>=e){
        return true;
    }
    if(input[s] != input[e]){
        return false;
    }
    return checkPalin(input,s+1,e-1);
}

bool checkPalin(char input[]){
    int e = strlen(input) - 1;
    return checkPalin(input, 0, e);
}

int main(){
    char input[50];
    cin>>input;
    bool val = checkPalin(input);
    val?cout<<"True"<<endl:cout<<"False"<<endl;
}