#include<iostream>
using namespace std;

int numCount(int n){
    if(n==0){
        return 0;
    }
    int val = n%10 + numCount(n/10);
    return val;
}

int main(){
    int n;
    cin>>n;
    int count = numCount(n);
    cout<<count<<endl;
}