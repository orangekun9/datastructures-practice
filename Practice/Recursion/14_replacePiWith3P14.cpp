#include<iostream>
#include<cstring>
using namespace std;

void replacePi(char input[]){
    int len = strlen(input);

    if(len == 0){
        return;
    }
    // Making space by shifting elements
    if(input[0] == 'p' && input[1] == 'i'){
        for(int i=len+2;i>0;i--){
            // its not input[i-2] = input[i] because input[i] is a null value.
            input[i] = input[i-2]; 
        }

        input[0] = '3';
        input[1] = '.';
        input[2] = '1';
        input[3] = '4';

        replacePi(input+4);
    }
    else{
        replacePi(input+1);
    }
}

int main(){
    char input[50];
    cin>>input;
    replacePi(input);
    cout<<input<<endl;
}