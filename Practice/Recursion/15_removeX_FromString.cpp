#include<iostream>
#include<cstring>
using namespace std;

void removeX(char input[]){
    int len = strlen(input);
    if(len == 0){
        return;
    }
    removeX(input+1);
    if(input[0] == 'x'){
        int i;
        for(i=0;i<=len;i++){
            int temp = input[i];
            input[i] = input[i+1];
            input[i+1] = input[i]; 
        }
        input[i-1] = '\0';
    }
}

int main(){
    char input[50];
    cin>>input;
    removeX(input);
    cout<<input<<endl;
}