#include<iostream>
#include<cstring>
using namespace std;

void pairStar(char input[]){
    int len = strlen(input);

    if(len == 0){
        return;
    }

    if(input[0] == input[1]){
        for(int i=len+1;i>1;i--){
            input[i] = input[i-1];
        }
        input[1] = '*';
        pairStar(input+2);
    }
    else{
        pairStar(input+1);
    }
}

int main(){
    char input[50];
    cin>>input;

    pairStar(input);

    cout<<input<<endl;
}