#include<iostream>
using namespace std;

void towerOfHanoi(int n, char source, char auxiliary, char destination){
    if(n==1){
        cout<<source<<" "<<destination<<endl;
        return;
    }
    if(n==0){
        return;
    }
    towerOfHanoi(n-1, source, destination, auxiliary);
    cout<<source<<" "<<destination<<endl;
    towerOfHanoi(n-1, auxiliary, source, destination);
}

int main(){
    char source = 'a', auxiliary = 'b', destination = 'c';
    int n;
    cin>>n;
    towerOfHanoi(n, source, auxiliary, destination);
}