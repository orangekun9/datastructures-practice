#include<iostream>
#include<cstring>
using namespace std;

void replaceChar(char input[], char val, char x){
    int len = strlen(input);
    if(len == 0){
        return;
    }

    if(input[0] == val){
        input[0] = x;
    }

    replaceChar(input+1, val, x);

}

int main(){
    char input[50], val1, val2;
    cin>>input;
    cout<<"Enter current value and replace value: "<<endl;
    cin>>val1>>val2;
    replaceChar(input, val1, val2);
    cout<<input;
}