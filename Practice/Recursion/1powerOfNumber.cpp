#include<iostream>
using namespace std;

int pow(int n, int p){
    // Base case
    if(n==0){
        return 1;
    }
    int val = p*pow(n-1,p);
    return val;
}

int main(){

    int num,power;
    cin>>num>>power;
    int val = pow(num, power);
    cout<<val<<endl;
}