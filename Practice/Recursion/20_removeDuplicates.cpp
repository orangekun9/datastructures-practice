#include<iostream>
#include<cstring>
using namespace std;

void removeDup(char input[]){
    int len = strlen(input);
    if(len == 0){
        return;
    }

    if(input[0] == input[1]){
        int i;
        for(i=1;i<=len;i++){
            input[i-1] = input[i];
        }
        removeDup(input);
    }
    removeDup(input+1);
}

int main(){
    char input[50];
    cin>>input;
    removeDup(input);
    cout<<input<<endl;
}