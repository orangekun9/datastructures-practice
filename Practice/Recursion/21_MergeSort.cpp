#include<iostream>
using namespace std;

void merge(int input[], int s, int e){
    int mid = (s+e)/2;
    int i = s;
    int j = mid+1;
    int arr[100],k=0;
    while(i<=mid && j<=e){
        if(input[i] <= input[j]){
            arr[k++] = input[i++];
        }
        else if(input[j] < input[i]){
            arr[k++] = input[j++];
        }
    }

    while(i<=mid){
        arr[k++] = input[i++];
    }
    while(j<=e){
        arr[k++] = input[j++];
    }

    for(int i=s,a=0;i<=e;i++,a++){
        input[i] = arr[a];
    }
}

void mergeSort(int input[], int s, int e){
    if(s>=e){
        return;
    }
    int mid = (s+e)/2;
    mergeSort(input, s, mid);
    mergeSort(input, mid+1, e);
    merge(input, s, e);
}

void mergeSort(int input[], int n){
    mergeSort(input, 0, n-1);
}

int main(){
    int input[50],n;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    mergeSort(input, n);
    
    for(int i=0;i<n;i++){
        cout<<input[i]<<" ";
    }
    cout<<endl;
}