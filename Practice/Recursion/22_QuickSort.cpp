#include<iostream>
using namespace std;

int partition(int input[], int s, int e){
    int val=0;
    for(int i=s+1;i<=e;i++){
        if(input[s] >= input[i]){
            val++;
        }
    }

    int pivot = s+val;

    int temp = input[pivot];
    input[pivot] = input[s];
    input[s] = temp;

    int i=s, j=e;

    while(i<pivot && j>pivot){
        if(input[i] <= input[pivot]){
            i++;
        }
        else if(input[j] > input[pivot]){
            j--;
        }
        else if(input[i] > input[pivot] && input[j] <= input[pivot]){
            int temp = input[i];
            input[i] = input[j];
            input[j] = temp;
            i++;
            j--;
        }
    }

    return pivot;
}

void quickSort(int input[], int s, int e){
    if(s>=e){
        return;
    }
    int pivot = partition(input, s, e);
    quickSort(input, s, pivot-1);
    quickSort(input, pivot+1, e);
}

void quickSort(int input[], int n){
    quickSort(input, 0, n-1);
}

int main(){
    int input[50],n;

    cin>>n;

    for(int i=0;i<n;i++){
        cin>>input[i];
    }

    quickSort(input, n);

    for(int i=0;i<n;i++){
        cout<<input[i]<<" ";
    }
    cout<<endl;
}