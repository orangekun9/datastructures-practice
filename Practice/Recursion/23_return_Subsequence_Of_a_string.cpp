#include<iostream>
using namespace std;

int subsq(string input, string output[]){
    if(input.empty()){
        output[0] = "";
        return 1;
    }
    int val = subsq(input.substr(1), output);
    for(int i=0;i<val;i++){
        output[val+i] = input[0] + output[i];
    }

    return val*2;
}

int main(){
    string p;
    string output[100];
    cin>>p;
    int val = subsq(p, output);
    for(int i=0;i<val;i++){
        cout<<output[i]<<endl;
    }
}