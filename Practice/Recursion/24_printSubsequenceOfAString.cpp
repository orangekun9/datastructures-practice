#include<iostream>
using namespace std;

void subsq(string input, string output){
    if(input.empty()){
        cout<<output<<endl;
        return;
    }
    subsq(input.substr(1), output);
    subsq(input.substr(1), output+input[0]);
}

int main(){
    string input, output="";
    cin>>input;
    subsq(input, output);
}