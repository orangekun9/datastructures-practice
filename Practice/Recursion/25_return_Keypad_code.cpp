#include<iostream>
using namespace std;

string key[] = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

int keypadCode(int input, string output[]){
    if(input == 0 || input == 1){
        output[0] = "";
        return 1;
    }
    string o[100];
    int val = keypadCode(input/10, o);
    string str = key[input%10];
    int k=0;
    for(int i=0;i<str.size();i++){
        for(int j=0;j<val;j++){
            output[k++] = o[j] + str[i];
        }
    }
    return k;
}

int main(){
    int input;
    string output[100];
    cin>>input;
    int val = keypadCode(input, output);
    for(int i=0;i<val;i++){
        cout<<output[i]<<endl;
    }
}