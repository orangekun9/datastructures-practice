#include<iostream>
using namespace std;

string key[] = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

void keypad(int input, string output){
    if(input == 0){
        cout<<output<<endl;
        return;
    }
    string op = key[input%10];
    for(int i=0;i<op.size();i++){
        keypad(input/10, op[i]+output);
    }
}

int main(){
    int input;
    cin>>input;
    string output="";
    keypad(input, output);
}