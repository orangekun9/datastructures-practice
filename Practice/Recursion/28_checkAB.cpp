#include<iostream>
using namespace std;

bool checkAB(char input[]){
    if(input[0] == '\0'){
        return true;
    }

    if(input[0] == 'a'){
        if(input[1] == 'b' && input[2] == 'b'){
            checkAB(input+3);
        }
        else if(input[1] == '\0' || input[1] == 'a'){
            checkAB(input+1);
        }
    }
    else if(input[0] != 'a'){
        return false;
    }
}

int main(){
    char input[50];
    cin>>input;
    bool val=checkAB(input);
    val?cout<<"True"<<endl:cout<<"False"<<endl;
}