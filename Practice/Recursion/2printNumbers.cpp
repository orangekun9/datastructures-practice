// ex: input=6 output= 1 2 3 4 5 6
#include<iostream>
using namespace std;

void print(int n){
    if(n==0){
        return;
    }
    print(n-1);
    cout<<n<<" ";
}

int main(){
    int n;
    cin>>n;

    print(n);
}