#include<iostream>
using namespace std;

int bSearch(int input[], int s, int e, int x){
    
    if(s<=e){
        int mid = (s+e)/2;
        if(x == input[mid]){
            return mid;
        }
        else if(x > input[mid]){
            bSearch(input, mid+1, e, x);
        }
        else if(x < input[mid]){
            bSearch(input, s, mid-1, x);
        }
    }
    else{
        return -1;
    }
    
}

int bSearch(int input[], int n, int x){
    return bSearch(input, 0, n-1, x);
}

int main(){
    int input[50],n,x;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    cin>>x;
    int val = bSearch(input, n, x);
    val!=-1?cout<<"Element present in the array in index position: "<<val<<endl:cout<<"Element is not present in the array"<<endl;
}