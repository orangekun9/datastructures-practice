#include<iostream>
using namespace std;

int subset(int input[], int n, int output[][100]){
    if(n==0){
        output[0][0] = 0;
        return 1;
    }

    int smallN = subset(input+1, n-1, output);

    // Copying first column
    for(int i=0;i<smallN;i++){
        output[i+smallN][0] = output[i][0]+1;
    }

    // Copying the remaining the column elements
    for(int i=0;i<smallN;i++){
        for(int j=1;j<=output[i][0];j++){
            output[i+smallN][j] = output[i][j];
        }
    }

    // Making space for the element
    for(int i=0;i<smallN;i++){
        for(int j=1;j<=output[i][0];j++){
            output[i+smallN][j+1] = output[i][j];
        }
    }

    // Putting the current element
    for(int i=0;i<smallN;i++){
        output[i+smallN][1] = input[0];
    }

    return 2*smallN;
}

int main(){
    int input[50],n,output[100][100];
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    int k = subset(input, n, output);
    for(int i=0;i<k;i++){
        for(int j=1;j<=output[i][0];j++){
            cout<<output[i][j]<<" ";
        }
        cout<<endl;
    }
}