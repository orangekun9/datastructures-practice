#include<iostream>
using namespace std;

void subset(int input[], int n, int output[], int m){
    if(n==0){
        for(int i=0;i<m;i++){
            cout<<output[i]<<" ";
        }
        cout<<endl;
        return;
    }
    int newOut[50],s=0;
    for(int i=0;i<m;i++,s++){
        newOut[s] = output[i];
    }
    newOut[s] = input[0];
    subset(input+1, n-1, newOut, m+1);
    subset(input+1,n-1,output,m);
}

int main(){
    int input[50],n,output[50];
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    subset(input, n, output, 0);
}