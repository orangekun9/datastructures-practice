#include<iostream>
using namespace std;

int sumK(int input[], int n, int k, int output[][100]){
    if(k<0 || n<0) return 0;
    if(n==0){
        if(k==0){
            output[0][0] = 0;
            return 1;
        }
        else{
            return 0;
        }
    }
    if(k==0){
        output[0][0] = 0;
        return 1;
    }
    int o1[100][100], o2[100][100];
    int s1 = sumK(input+1, n-1, k-input[0], o1);
    int s2 = sumK(input+1, n-1, k, o2);

    for(int i=0;i<s1;i++){
        output[i][0] = o1[i][0]+1; // Copying the first element of array
        output[i][1] = input[0]; // Adding the current element
        for(int j=1;j<=o1[i][0];j++){
            output[i][j+1] = o1[i][j]; // Copying the remaining elements and also making space at the same time
        }
    }

    for(int i=s1;i<s1+s2;i++){
        for(int j=0;j<=o2[i-s1][0];j++){
            output[i][j] = o2[i-s1][j];
        }
    }
    return s1+s2;
}
  
int main(){
    int input[50], n, k, output[100][100];
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    cin>>k;
    int val = sumK(input, n, k, output);

    for(int i=0;i<val;i++){
        for(int j=1;j<=output[i][0];j++){
            cout<<output[i][j]<<" ";
        }
        cout<<endl;
    }

}