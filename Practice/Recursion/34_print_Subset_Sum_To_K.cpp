#include<iostream>
using namespace std;

void sumK(int input[], int n, int output[], int k, int m){
    if(n==0){
        if(k==0){
            for(int i=0;i<m;i++){
                cout<<output[i]<<" ";
            }
            cout<<endl;
        }
        return;
    }

    int o[100],i;
    sumK(input+1, n-1, output, k, m);
    
    // Copying the output elements
    if(k>0){
        for(i=0;i<m;i++){
            o[i] = output[i];
        }
        o[i] = input[0];

        sumK(input+1, n-1, o, k-input[0], m+1);
    }
}

int main(){
    int input[50],n,output[100],k;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    cin>>k;
    sumK(input, n, output, k, 0);
}