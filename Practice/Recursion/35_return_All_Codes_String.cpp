#include<iostream>
#include<string.h>
using namespace std;

int allCodes(string input, string output[]){
    if(input.empty()){
        output[0] = "";
        return 1;
    }
    int first = input[0]-48;
    char firstChar = first + 'a' - 1;
    char secondChar='\0';
    string res1[1000], res2[1000];
    int s1 = allCodes(input.substr(1), res1);
    int s2 = 0;
    if(input[1] != '\0'){
        int second = (first * 10)+ input[1] - 48;
        if(second >= 10 && second <= 26){
            secondChar = second + 'a' - 1;
            s2 = allCodes(input.substr(2), res2);
        }
    }

    int k=0;

    for(int i=0;i<s1;i++,k++){
        output[k] = firstChar + res1[i];
    }

    for(int i=0;i<s2;i++,k++){
        output[k] = secondChar + res2[i];
    }

    return k;
}

int main(){
    string input, output[100];
    cin>>input;
    int val = allCodes(input, output);
    for(int i=0;i<val;i++){
        cout<<output[i]<<endl;
    }
}