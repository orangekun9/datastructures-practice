#include<iostream>
using namespace std;

void allCodes(string input, string output){
    if(input.empty()){
        cout<<output<<endl;
        return;
    }
    int first = input[0] - 48;
    char firstChar = first+'a'- 1;
    allCodes(input.substr(1), output + firstChar);
    if(input[1] != '\0'){
        int second = (first*10) + input[1] - 48;
        if(second>=10 && second<=26){
            char secondChar = second + 'a' - 1;
            allCodes(input.substr(2), output + secondChar);
        }
    }
}

int main(){
    string input, output="";
    cin>>input;
    allCodes(input, output);
}