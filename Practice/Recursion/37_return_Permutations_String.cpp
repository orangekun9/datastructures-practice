#include<iostream>
using namespace std;

int strPer(string input, string output[]){
    if(input.empty()){
        output[0] = "";
        return 1;
    }
    string smallOutput[1000];
    int smallSize = strPer(input.substr(1), smallOutput);
    int k=0;
    for(int i=0; i<smallSize; i++){
        for(int j=0; j<=smallOutput[i].length();j++){
            output[k++] = smallOutput[i].substr(0,j) + input[0] + smallOutput[i].substr(j);
        }
    }
    return k;
}

int main(){
    string input, output[1000];
    cin>>input;
    int val = strPer(input, output);
    for(int i=0;i<val;i++){
        cout<<output[i]<<endl;
    }
}