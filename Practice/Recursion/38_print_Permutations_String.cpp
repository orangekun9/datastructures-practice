#include<iostream>
using namespace std;

void strPer(string input, string output){
    if(input.empty()){
        cout<<output<<endl;
        return;
    }

    for(int i=0;i<input.length();i++){
        strPer( input.substr(0,i)+input.substr(i+1), output+input[i] );
    }
}

int main(){
    string input, output="";
    cin>>input;
    strPer(input, output);
}