// Count number of digits input = 123 output = 3
#include<iostream>
using namespace std;

int count(int n){
    if(n==0){
        return 0;
    }
    int val = 1+count(n/10);
    return val;
}

int main(){
    int n;
    cin>>n;
    int val = count(n);
    cout<<val<<endl;
}