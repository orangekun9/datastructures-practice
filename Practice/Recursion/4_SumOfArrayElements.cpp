// Sum of elements of array input= 3   1 2 3    output=6
#include<iostream>
using namespace std;

int sumCount(int input[], int n){
    if(n==0){
        return 0;
    }
    
    int val = input[0] + sumCount(input+1,n-1);

    return val;

}

int main(){
    int input[100],n;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    int count = sumCount(input, n);
    cout<<count<<endl;
}