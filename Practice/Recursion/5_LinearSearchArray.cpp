// Linear Search using recursion
#include<iostream>
using namespace std;

bool search(int input[], int n, int x){
    if(n==0){
        return false;
    }
    if(input[0] == x){
        return true;
    }
    return search(input+1, n-1, x);
}

int main(){
    int arr[50],n,x;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    cout<<"Enter number to find: ";
    cin>>x;
    bool val = search(arr, n, x);
    val?cout<<"true"<<endl:cout<<"false"<<endl;
}