#include<iostream>
using namespace std;

int firstIndex(int input[], int n, int x){
    if(n==0){
        return -1;
    }
    if(input[0] == x){
        return 0;
    }
    int val = 1+firstIndex(input+1, n-1, x);
    if(val>0){
        return val;
    }
    else{
        return -1;
    }
}

int main(){
    int input[100],n,x;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    cin>>x;
    int index = firstIndex(input, n, x);
    cout<<"First occurance of element "<<x<<" is at index: "<<index<<endl;
}