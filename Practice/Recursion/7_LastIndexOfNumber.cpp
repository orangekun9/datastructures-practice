#include<iostream>
using namespace std;

int lastIndex(int input[], int n, int x){
    if(n==0){
        return -1;
    }
    int val = lastIndex(input+1,n-1,x);
    if(val == -1){
        if(input[0] == x){
            return 0;
        }
        else{
            return -1;
        }
    }
    else{
        return val+1;
    }
}

int main(){
    int input[100],n,x;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    cin>>x;
    int index = lastIndex(input, n, x);
    cout<<"Last occurance of element "<<x<<" is at index: "<<index<<endl;
}