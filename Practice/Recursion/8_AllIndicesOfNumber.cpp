#include<iostream>
using namespace std;

void findIndexes(int input[], int n, int x, int output[], int &k, int currentIndex){
    if(n==0){
        return;
    }

    if(input[0] == x){
        output[k++] = currentIndex; 
    }
    findIndexes(input+1, n-1, x, output, k, currentIndex+1);
}

int allIndexes(int input[], int n, int x, int output[]){
    int k=0;
    findIndexes(input, n, x, output, k, 0);
    return k;
}

int main(){
    int input[100],n,x,output[100];
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>input[i];
    }
    cin>>x;
    int val = allIndexes(input, n, x, output);
    for(int i=0;i<val;i++){
        cout<<output[i]<<" ";
    }
    cout<<endl;
}