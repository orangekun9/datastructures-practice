// Logic is to add n m times
#include<iostream>
using namespace std;

int mul(int n, int m){
    if(m==0){
        return 0;
    }
    int val = n+mul(n,m-1);
    return val;
}

int main(){
    int n,m;
    cin>>n>>m;
    int val = mul(n,m);
    cout<<val<<endl;
}