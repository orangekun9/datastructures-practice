#include<iostream>
using namespace std;

class node{
    public:
    int data;
    node* next;

    node(int data){
        this->data = data;
        next = NULL;
    }
};

node* inputList(){
    int data;
    cin>>data;
    node* head = NULL, *tail = NULL;
    while(data != -1){
        node* newNode = new node(data);
        if(head == NULL){
            head = newNode;
            tail = newNode;
        }
        else{
            tail->next = newNode;
            tail = tail->next;
        }
        cin>>data;
    }
    return head;
}

void print(node* head){
    if(head == NULL){
        return;
    }
    cout<<head->data<<" ";
    print(head->next);
}

node* merge(node* head1, node* head2){
    node* fh=NULL, *ft=NULL;
    while(head1 != NULL && head2 != NULL){
        if(head1->data <= head2->data){
            if(fh == NULL){
                fh = head1;
                ft = head1;
                head1 = head1->next;
            }
            else{
                ft->next = head1;
                ft = ft->next;
                head1 = head1->next;
            }
        }

        else if(head2->data < head1->data){
            if(fh == NULL){
                fh = head2;
                ft = head2;
                head2 = head2->next;
            }
            else{
                ft->next = head2;
                ft = ft->next;
                head2 = head2->next;
            }
        }
    }

    while(head1 != NULL){
        ft->next = head1;
        ft = ft->next;
        head1 = head1->next;
    }

    while(head2 != NULL){
        ft->next = head2;
        ft = ft->next;
        head2 = head2->next;
    }

    return fh;
}

int main(){
    node* head1 = inputList();
    cout<<endl;
    node* head2 = inputList();
    cout<<endl;
    node* head3 = merge(head1, head2);
    print(head3);
}