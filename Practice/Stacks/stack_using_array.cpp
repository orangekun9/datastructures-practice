#include<iostream>
#include<climits>
using namespace std;

class Stack{
    int* data;
    int nextIndex;
    int capacity;

    public:
        Stack(){
            data = new int[5];
            nextIndex = 0;
            capacity = 5;
        }

        // Getting the size of the stack
        int size(){
            return nextIndex;
        }

        bool isEmpty(){
            return nextIndex==0;
        }

        // Inserting a element in stavk
        void push(int element){
            if(nextIndex == capacity){
               int*  newData = new int[2*capacity];
               for(int i=0;i<capacity;i++){
                   newData[i] = data[i];
               }
                delete [] data;
                data = newData;
                capacity *= 2;
            }
            data[nextIndex++] = element;
        }

        // Deleting an element in stack
        int pop(){
            if(isEmpty()){
                cout<<"Stack is empty"<<endl;
                return INT_MIN;
            }
            return data[nextIndex--];
        }

        // Return first element in the stack
        int top(){
            if(isEmpty()){
                cout<<"Stack is empty"<<endl;
                return 0;
            }
            return data[nextIndex-1];
        }
};

int main(){
    Stack s;

    s.top()==0?cout<<"":cout<<s.top()<<endl;

    s.push(1);
    s.push(2);
    s.push(3);
    s.push(4);
    s.push(5);
    s.push(6);

    s.top()==0?cout<<"":cout<<s.top()<<endl;
    cout<<s.size()<<endl;
    s.isEmpty()?cout<<"Stack is empty"<<endl:cout<<"Stack is not empty"<<endl;
}