#include<iostream>
using namespace std;

void inplace_heap_sort(int input[], int n){
    // Build the heap in an input array
    for(int i=1;i<n;i++){
        int childIndex = i;
        while(childIndex > 0){
            int parentIndex = (childIndex - 1)/2;
            if(input[childIndex] < input[parentIndex]){
                int temp = input[childIndex];
                input[childIndex] = input[parentIndex];
                input[parentIndex] = temp;
            }
            else{
                break;
            }
            childIndex = parentIndex;
        }
    }

    // Remove elements
    int size = n;
    while(size>0){

        int temp = input[0];
        input[0] = input[size-1];
        input[size-1] = temp;
        size--;

        int parentIndex = 0;
        int leftChildIndex = (parentIndex*2)+1;
        int rightChildIndex = (parentIndex*2)+2;
        int minIndex;
        while(leftChildIndex < size){
            minIndex = parentIndex;
            if(input[leftChildIndex] < input[minIndex]){
                minIndex = leftChildIndex;
            }

            if(rightChildIndex < size && input[rightChildIndex] < input[minIndex]){
                minIndex = rightChildIndex;
            }

            if(minIndex == parentIndex){
                break;
            }
            int temp = input[minIndex];
            input[minIndex] = input[parentIndex];
            input[parentIndex] = temp;
            parentIndex = minIndex;
            leftChildIndex = (parentIndex*2)+1;
            rightChildIndex = (parentIndex*2)+2;
        }
    }
}

int main(){
    int arr[] = {5,1,2,0,8};
    inplace_heap_sort(arr, 5);
    for(int i=0;i<5;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}