#include<iostream>
#include<vector>
using namespace std;

// Implementing MIN-HEAP
class PriorityQueue{
    vector<int> pq;

    public:
    
    bool isEmpty()[
        return pq.size() == 0;
    ]

    // Returns the size of the priority queue or we can say number of elements 
    int getSize(){
        return pq.size();
    }

    int getMin(){
        if(pq.size() == 0){
            return 0;   // Priority queue is empty
        }
        return pq[0];
    }

    void insert(int element){
        pq.push_back(element);

        int childIndex = pq.size()-1;
        
        while(childIndex > 0){
            int parentIndex = (childIndex-1)/2;     // This is the formula for getting the parent of an index

            if(pq[childIndex] < pq[parentIndex]){
                int temp = pq[childIndex];
                pq[childIndex] = pq[parentIndex];
                pq[parentIndex] = temp;
            }
            else{
                break;
            }

            childIndex = parentIndex;
        }
    }

    int removeMin(){
        if(isEmpty()){
            return 0;
        }
        int ans = pq[0];
        pq[0] = pq[pq.size()-1];
        pq.pop_back();

        // Down heapify
        int parentIndex = 0;
        int leftChildIndex = (2*parentIndex) + 1;
        int rightChildIndex = (2*parentIndex) + 2;
        int minIndex;
        while(leftChildIndex < pq.size()){
            minIndex = parentIndex;
            if(pq[minIndex] > pq[leftChildIndex]){
                minIndex = leftChildIndex;
            }

            if(rightChildIndex < pq.size() && pq[minIndex] > pq[rightChildIndex]){
                minIndex = rightChildIndex;
            }

            if(minIndex == parentIndex){
                break;
            }

            int temp = pq[minIndex];
            pq[minIndex] = pq[parentIndex];
            pq[parentIndex] = temp;
            parentIndex = minIndex;
            leftChildIndex = (2*parentIndex) + 1;
            rightChildIndex = (2*parentIndex) + 2;
        }
        return ans;
    }

};