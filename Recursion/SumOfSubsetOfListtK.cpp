#include<iostream>
using namespace std;

int subsetSumToK(int input[], int n, int output[][50], int k) { 
    if(k<0||n<0) return 0; 
    if(n==0) { 
        if(k==0) {
            output[0][0]=0; 
            return 1;
        } 
        else return 0; 
    }
    if(k==0){
        output[0][0]=0;
        return 1;
    }
    
    int t1[1000][50]={{0}},t2[1000][50]={};
    int s1=subsetSumToK(input+1,n-1,t1,k-input[0]);
    int s2=subsetSumToK(input+1,n-1,t2,k);
    for(int i=0;i<s1;i++) { 
        output[i][0]=t1[i][0]+1; 
        output[i][1]=input[0]; 
        for(int j=1;j<=t1[i][0];j++) 
            output[i][j+1]=t1[i][j]; 
    } 
    for(int i=s1;i<s1+s2;i++) {
        for(int j=0;j<=t2[i-s1][0];j++)
            output[i][j]=t2[i-s1][j]; 
    } 
    return s1+s2; 
}

int main(){
    int input[] = {1,2,3,4};
    int output[1000][50];
    int count = subsetSumToK(input,4,output,5);
    for(int i=0;i<count;i++){
        int temp=output[i][0];
        for(int j=0;j<=temp;j++){
            cout<<output[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<"Hello World"<<endl;
}