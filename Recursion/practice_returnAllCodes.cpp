#include<iostream>
using namespace std;

int getCode(string input, string output[100]){
    if(input.empty()){
        output[0] = "";
        return 1;
    }

    int first = input[0] - 48;
    char str1 = first+'a'-1;
    char str2='\0';
    string res1[1000];
    string res2[1000];
    int size1 = getCode(input.substr(1), res1);
    int size2=0;

    if(input[1] != '\0'){
        int second = (first*10)+input[1]-48;
        if(second>=10 && second<=26){
            str2 = second+'a'-1;
            size2 = getCode(input.substr(2), res2);
        }
    }

    int k=0;
    for(int i=0;i<size1;i++,k++){
        output[k] = str1 + res1[i];
    }
    for(int i=0;i<size2;i++,k++){
        output[k] = str2 + res2[i];
    }

    return k;
}

int main(){
    string input;
    cin>>input;
    string output[1000];
    int count = getCode(input, output);
    for(int i=0;i<count;i++){
        cout<<output[i]<<endl;
    }
}