#include <iostream>
#include <string>
using namespace std;

void helperFunc(string input, string output){
    
    if(input.size()==0){
        cout<<output<<endl;   
    }
    
    for(int i=0;i<input.size();i++){
        helperFunc(input.substr(0,i)+input.substr(i+1),output+input[i]);
    }
    
}

void printPermutations(string input){

    string output="";
    helperFunc(input, output);
}


int main(){
    string input;
    cin>>input;

    printPermutations(input);
}