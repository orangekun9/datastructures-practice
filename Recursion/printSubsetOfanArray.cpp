#include<iostream>
using namespace std;
void helperFunc(int input[], int n, int output[], int m){
    if(n == 0){
        for(int i=0;i<m;i++){
            cout<<output[i]<<" ";
        }
        cout<<endl;
        return;
    }
    
    int newOutput[100],s=0;
    
    for(int i=0;i<m;i++,s++){
        newOutput[s] = output[i];
    }
    newOutput[s] = input[0];
    
    helperFunc(input+1,n-1,newOutput,m+1);
    helperFunc(input+1,n-1,output,m);
}

void printSubsetsOfArray(int input[], int size) {   
	// Write your code here
    int output[100];
    helperFunc(input,size,output,0);
}

int main(){
    int input[] = {1,2,3,4};
    printSubsetsOfArray(input,4);

}