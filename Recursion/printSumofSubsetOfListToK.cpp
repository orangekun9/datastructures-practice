#include<iostream>
using namespace std;

void printSum(int input[], int n, int k, int output[], int m){
    if(n==0){
        if(k==0){
            for(int i=0;i<m;i++){
                cout<<output[i]<<" ";
            }
            cout<<endl;
        }
        return;
    }

    int o[100],i;
    printSum(input+1, n-1, k, output, m);
    if(k>0){
        for(i=0;i<m;i++){
            o[i] = output[i];
        }
        o[i] = input[0];
        printSum(input+1, n-1, k-input[0], o, m+1);
    }
}

void printSum(int input[], int n, int k){
    int output[100];
    printSum(input, n, k, output, 0);
}

int main(){
    int input[] = {1,2,3,4,5,6};
    printSum(input, 6, 5);
}