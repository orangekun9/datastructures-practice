#include<iostream>
using namespace std;

int returnPermutation(string input, string output[]){
    if(input.empty()){
        output[0] = "";
        return 1;
    }
    string smallOutput[1000];
    int size = returnPermutation(input.substr(1), smallOutput);

    int k=0;
    for(int i=0;i<size;i++){
        for(int j=0;j<=smallOutput[i].length();j++){
            output[k++] = smallOutput[i].substr(0,j) + input[0] + smallOutput[i].substr(j);
        }
    }
    return k;
}

int main(){
    string input;
    cin>>input;
    string output[1000];
    int count = returnPermutation(input, output);
    cout<<"-----------------------------"<<endl;
    for(int i=0;i<count;i++){
        cout<<output[i]<<endl;
    }
}