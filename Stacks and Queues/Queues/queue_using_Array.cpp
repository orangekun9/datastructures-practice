#include<iostream>
using namespace std;

template <typename T>
class Queue{
    T* data;
    int nextIndex;
    int firstIndex;
    int size;
    int capacity;

    public:
        Queue(int s){
            data = new T[s];
            nextIndex = 0;
            firstIndex = -1;
            size=0;
            capacity = s;
        }

        int getSize(){
            return size;
        }

        bool isEmpty(){
            return size==0;
        }

        void enqueue(T element){
            if (size == capacity){
                cout<<"Queue is full"<<endl;
                return;
            }
            data[nextIndex] = element;
            nextIndex = (nextIndex+1)%capacity;
            if(firstIndex == -1){
                firstIndex = 0;
            }
            size++;
        }

        T front(){
            if(isEmpty()){
                cout<<"Queue is empty"<<endl;
                return 0;
            }
            return data[firstIndex];
        }

        T dequeue(){
            if(isEmpty()){
                cout<<"Queue is empty"<<endl;
                return 0;
            }
            T ans = data[firstIndex];
            firstIndex = (firstIndex+1)%capacity;
            size--;
            if(size==0){
                firstIndex = -1;
                nextIndex = 0;
            }
            return ans;
        }
};


int main(){
    Queue<int> q(5);

    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(3);
    q.enqueue(4);
    q.enqueue(5);
    q.enqueue(6);
    cout<<q.front()<<endl;
    q.dequeue();
    cout<<q.front()<<endl;
    
}