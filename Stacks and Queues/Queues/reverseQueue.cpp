#include<iostream>
#include<queue>
using namespace std;

void reverseQueue(queue<int> &q){
    if(q.size() == 0 || q.size() == 1){
        return;
    }
    int ans = q.front();
    q.pop();
    reverseQueue(q);
    q.push(ans);
}

int main(){
    queue<int> q;
    q.push(1);
    q.push(2);
    q.push(3);
    q.push(4);
    reverseQueue(q);
    while(q.size()!=0){
        cout<<q.front()<<" ";
        q.pop();
    }
    cout<<endl;
}