#include<iostream>
#include<stack>
using namespace std;

void reverseStack(stack<int> &input, stack<int> &extra){
    
    if(input.size() == 0 || input.size() == 1){
        return;
    }
    
    int ans = input.top();
    input.pop();
    reverseStack(input, extra);

    while(input.size() > 0){
        extra.push(input.top());
        input.pop();
    }

    input.push(ans);

    while(extra.size() > 0){
        input.push(extra.top());
        extra.pop();
    }

}

int main(){
    stack<int> input;
    stack<int> extra;
    input.push(1);
    input.push(2);
    input.push(3);
    input.push(4);
    int i=0;
    cout<<endl;
    reverseStack(input, extra);
    while(input.size()!=0){
        cout<<input.top()<<" ";
        input.pop();
    }
}