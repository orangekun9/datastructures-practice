#include<iostream>
#include<climits>
using namespace std;

template <typename T>

class StackUsingArray{
    T *data;
    int nextIndex;
    int capacity;

    public:
        StackUsingArray(){
            data = new T[5];
            nextIndex = 0;
            capacity = 5;
        }

        // Return the number of elements present in my stack
        int size(){
            return nextIndex;
        }

        // Return True if stack is empty and retur False if stack is not empty
        bool isEmpty(){
            // if(nextIndex==0){
            //     return true;
            // }
            // else{
            //     return false;
            // }

            // OR

            return nextIndex == 0;
        }

        // Insert element
        void push(T element){
            if(nextIndex == capacity){
               T*  newData = new T[2*capacity];
               for(int i=0;i<capacity;i++){
                   newData[i] = data[i];
               }
                delete [] data;
                data = newData;
                capacity *= 2;
            }
            data[nextIndex] = element;
            nextIndex++;
        }

        // Delete element
        int pop(){
            if(isEmpty()){
                cout<<"Stack is empty"<<endl;
                return 0;    
            }
            nextIndex--;
            return data[nextIndex];
        }

        // Getting the top most element
        int top(){
            if(isEmpty()){
                cout<<"Stack is empty"<<endl;
                return 0;
            }
            return data[nextIndex-1];
        }

};

int main(){
    // StackUsingArray<int> s1;

    // s1.push(1);
    // s1.push(2);
    // s1.push(3);
    // s1.push(4);
    // s1.push(5);
    // s1.push(6);
    
    // cout<<s1.top()<<endl;

    // cout<<s1.pop()<<endl;

    // cout<<s1.top()<<endl;
    
    // s1.isEmpty()?cout<<"Stack is empty"<<endl:cout<<"Stack is not empty"<<endl;

    StackUsingArray<char> s1;

    s1.push('a');
    s1.push('b');
    s1.push('c');
    s1.push('d');
    s1.push('e');
    s1.push('f');
    
    cout<<s1.top()<<endl;

    cout<<s1.pop()<<endl;

    cout<<s1.top()<<endl;
    
    s1.isEmpty()?cout<<"Stack is empty"<<endl:cout<<"Stack is not empty"<<endl;
}