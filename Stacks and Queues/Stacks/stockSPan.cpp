#include<iostream>
#include<stack>
using namespace std;

int* stockSpan(int price[], int size){
    stack<int> index;
    index.push(0);
    int *span = new int[size];
    span[0] = 1;
    int j=1;

    for(int i=1;i<size;i++){
        while(index.size()!=0 && price[i] > price[index.top()]){
            index.pop();
        }
        if(index.empty()){
            span[j++] = i+1;
        }
        else{
            span[j++] = i - index.top();
        }
        index.push(i);
    }
    return span;
}

int main(){
    int arr[8] = {60,70,80,100,90,75,80,120};
    int *newArr = stockSpan(arr,8);
    for(int i=0;i<8;i++){
        cout<<newArr[i]<<" ";
    }
}