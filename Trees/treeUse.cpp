#include<iostream>
#include "treeNode.h"
#include<queue>
using namespace std;

TreeNode<int>* takeInputLevelWise(){
    int rootData;
    cout<<"\nEnter Data: ";
    cin>>rootData;
    TreeNode<int>* root = new TreeNode<int>(rootData);
    queue<TreeNode<int>*> pendingNodes;

    pendingNodes.push(root);
    while (pendingNodes.size() != 0){
        TreeNode<int>* front = pendingNodes.front();
        pendingNodes.pop();
        cout<<"\nEnter num of children of "<<front->data<<": ";
        int numChild;
        cin>>numChild;

        for(int i=0;i<numChild;i++){
            int childData;
            cout<<"\nEnter "<<i<<"th child of "<<front->data<<": ";
            cin>>childData;
            TreeNode<int>* child = new TreeNode<int>(childData);
            front->children.push_back(child);
            pendingNodes.push(child);
        }
    }
    return root;   
}


TreeNode<int>* nextLarger(TreeNode<int>* root, int n){
    if(root == NULL){
        return root;
    }

    int currentLarge = 0;
    TreeNode<int>* ans = NULL;

    if(root->data > n){
        currentLarge = root->data;
        ans = root;
    }

    TreeNode<int>* smallAns = NULL;

    for(int i=0;i<root->children.size();i++){
        smallAns = nextLarger(root->children[i], n);

        if(smallAns != NULL){
            if(ans == NULL){
                ans = smallAns;
                currentLarge = smallAns->data;
            }
            
            if(smallAns->data < currentLarge){
                currentLarge = smallAns->data;
                ans = smallAns;
            }
        }
    }

    return ans;
}

void printAtLevelK(TreeNode<int>* root, int k){
    if(root == NULL){
        return;
    }

    if(k==0){
        cout<<root->data<<endl;
        return;
    }

    for(int i=0;i<root->children.size();i++){
        printAtLevelK(root->children[i], k-1);
    }
}

int numNodes(TreeNode<int>* root){
    if(root == NULL){
        return 0;
    }
    int ans=1;
    for(int i=0;i<root->children.size();i++){
        ans += numNodes(root->children[i]);
    }
    return ans;
}

void preOrder(TreeNode<int>* root){
    if(root == NULL){
        return;
    }
    cout<<root->data<<" ";
    for(int i=0;i<root->children.size();i++){
        preOrder(root->children[i]);
    }
}

int heightOfTree(TreeNode<int>* root){
    if(root == NULL){
        return 0;
    }
    int h=0;

    for(int i=0;i<root->children.size();i++){
        int temp = heightOfTree(root->children[i]);
        if(h<temp){
            h = temp;
        }
    }
    return h+1;
}

TreeNode<int>* takeInput(){
    int rootData;
    cout<<"\nEnter Data: ";
    cin>>rootData;

    TreeNode<int>* root = new TreeNode<int>(rootData);
    int n;
    cout<<"\nEnter number of children of "<<rootData<<" : ";
    cin>>n;

    for(int i=0;i<n;i++){
        TreeNode<int>* child = takeInput();
        root->children.push_back(child);
    }

    return root;
}

void printTree(TreeNode<int>* root){
    if(root == NULL){
        return;
    }
    cout<<root->data<<": ";
    for(int i=0;i<root->children.size();i++){
        cout<<root->children[i]->data<<", ";
    }
    cout<<endl;
    for(int i=0;i<root->children.size();i++){
        printTree(root->children[i]);
    }
}

int main(){

    // Tree: 1 4 2 30 3 11 0 0 0 0

    // TreeNode<int>* root = new TreeNode<int>(1);
    // TreeNode<int>* node1 = new TreeNode<int>(2);
    // TreeNode<int>* node2 = new TreeNode<int>(3);
    // root->children.push_back(node1);
    // root->children.push_back(node2);

    // TreeNode<int>* root = takeInput();

    TreeNode<int>* root = takeInputLevelWise();
    int nodes = numNodes(root);
    cout<<endl;
    printTree(root);

    cout<<"\nNumber of nodes: "<<nodes<<endl;

    printAtLevelK(root, 2);

    cout<<"\nHeight of the Tree: "<<heightOfTree(root)<<endl;

    cout<<"PreOrder traversal: "<<endl;
    preOrder(root); 

    int n;
    cout<<"\nEnter element to find its next largest in tree: ";
    cin>>n;

    cout<<"Next larger of "<<n<<": "<<nextLarger(root,n)->data<<endl;

    // TODO delete the tree
    delete root;
}