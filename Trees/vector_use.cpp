#include<iostream>
#include<vector>
using namespace std;

int main(){
    // Static Allocation
    vector<int> v;

    // Dynamic Allocation
    // vector<int>* v1 = new vector<int>();

    // For insertion
    v.push_back(10);
    v.push_back(20);
    v.push_back(30);
    v.push_back(40);
    v.push_back(50);

    //Remove the last element
    // v.pop_back();


    for(int i=0;i<v.size();i++){
        cout<<v[i]<<endl;
    }

    cout<<v.capacity()<<endl;


    // This is not safe because if we try to access index no. which is out of bound it won't give any error it'll print the value instead
    // cout<<v[0]<<endl;
    // cout<<v[1]<<endl;

    // v[0] = 100;

    // This is safe, it'll give error if you try to access index no. which is out of bound.
    // v.at(1);
}