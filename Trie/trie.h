#include "trieNode.h"
#include<string>

class Trie{
    private:

        TrieNode* root;

        void insert(TrieNode *root, string word){
                // Base case
                if(word.size() == 0){
                    root->isTerminal = true;
                    return;
                }

                // Small Calculation
                int index = word[0] - 'a';
                TrieNode* child;
                if(root->children[index] != NULL){
                    child = root->children[index];
                }
                else{
                    child = new TrieNode(word[0]);
                    root->children[index] = child;
                }

                // Recursive Call
                insert(child, word.substr(1));
        }

        bool search(TrieNode *root, string word){
            // Base case
            if(word.size() == 0){
                return root->isTerminal;
            }

            int index = word[0] - 'a';

            if(root->children[index] != NULL){
                return search(root->children[index], word.substr(1));
            }
            else{
                return false;
            }
        }

        void removeWord(TrieNode *root, string word){
            // Base case
            if(word.size() == 0){
                root->isTerminal = false;
                return;
            }

            // Small Calculation
            TrieNode* child;
            int index = word[0] - 'a';
            if(root->children[index] != NULL){
                child = root->children[index];
            }
            else{
                // Word not found
                return;
            }

            removeWord(child, word.substr(1));


            // Remove child node if useless
            if(child->isTerminal == false){
                for(int i=0;i<26;i++){
                    if(child->children[i] != NULL){
                        return;
                    }
                }

                delete child;
                root->children[index] = NULL;
            }
        }


    public:

        Trie(){
            root = new TrieNode('\0');
        }

        void insertWord(string word){
            insert(root, word);
        }

        bool search(string word){
            return search(root, word);
        }

        void removeWord(string word){
            removeWord(root, word);
        }
};